import AsyncStorage from '@react-native-async-storage/async-storage';
import { useWalletConnect, withWalletConnect,WalletServices } from '@walletconnect/react-native-dapp';
import * as React from 'react';
import {Button} from 'react-native';
import SplashScreen from 'react-native-splash-screen'

function App(): JSX.Element {
  React.useEffect(() => {
    SplashScreen.hide()
  }, [])
  
  const connector = useWalletConnect();
  //console.log(WalletServices);
  if (!connector.connected) {
    /**
     *  Connect! 🎉
     */
    return <Button title="Connect" onPress={() => connector.connect()} />;
  }
  return <Button title="Kill Session" onPress={() => connector.killSession()} />;
}



export default withWalletConnect(App, {
  redirectUrl: Platform.OS === 'web' ? window.location.origin : 'yourappscheme://',
  storageOptions: {
    asyncStorage:AsyncStorage,
  },
});