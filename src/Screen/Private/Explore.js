import { View, Text, StyleSheet, TextInput, FlatList, Image } from 'react-native'
import React, { useState } from 'react'
import ViewContainer from '../../Components/HOC/ViewContainer'
import ScrollContainer from '../../Components/HOC/ScrollContainer';
import Typography from '../../Components/UI/Typography';
import Icon from '../../Components/UI/Icon';
import Images from '../../Constants/images';
import Press from '../../Components/HOC/Press';
import CommonButton from '../../Components/UI/CommonButton';
import CommonInput from '../../Components/UI/CommonInput';
import AuthHeader from '../../Components/HOC/AuthHeader';



const Explore = ({ navigation }) => {

    const [list, setList] = useState([

        {
            img: Images.Layer9, img1: Images.music,
            text: 'Hip Hop', text1: '7.3M Tracks'
        },
        {
            img: Images.Layer7, img1: Images.music,
            text: 'Electonics', text1: '02M Tracks'
        },
        {
            img: Images.Layer7, img1: Images.music,
            text: 'Electonics', text1: '02M Tracks'
        },
        {
            img: Images.Layer9, img1: Images.music,
            text: 'Hip Hop', text1: '7.3M Tracks'
        },


    ])


    const [list1, setList1] = useState([

        { imgA: Images.pic1, textA: 'Urna Rhoncusssssssss', textB: 'Raybeats', imgB: Images.check },
        { imgA: Images.pic2, textA: 'Potenti Euismod', textB: 'Raybeats', imgB: Images.check },
        { imgA: Images.pic3, textA: 'Vestibulum Erae', textB: 'Raybeats', imgB: Images.check},
        { imgA: Images.pic1, textA: 'Vestibulum Erate', textB: 'Raybeats', imgB: Images.check },
        { imgA: Images.pic2, textA: 'Potenti Euismod', textB: 'Raybeats', imgB: Images.check }

    ])

    const [list2, setList2] = useState([

        { imgC:Images.Layer1, textC: '$09.00', textD: 'Romantic of tortorrrrrrrrr',textE:"Raybeats" , imgB: Images.check},
        { imgC: Images.Layer3, textC: '$10.00', textD: 'Romantic of tortorrrrrrrr',textE:"Raybeats" , imgB: Images.check},
        { imgC: Images.Layer5, textC: '$39.00', textD: 'Romantic of tortorrrrrrr',textE:"Raybeats" , imgB: Images.check },
        { imgC: Images.Layer1, textC: '$9.00', textD: 'Romantic of tortorrrrrrr',textE:"Raybeats" , imgB: Images.check },
        { imgC: Images.Layer5, textC: '$19.00' , textD: 'Romantic of tortorrrrrrr',textE:"Raybeats" , imgB: Images.check},



    ])


    return (
        <ScrollContainer withBG  
        onBottomPress={() => {navigation.navigate('Notification')}}
        bottomButton={false}
        bottomView
        >
            <AuthHeader
                title={'Explore'}
                shopIcon
                filterIcon
                style />
            <View style={styles.searchBox}>

                <View style={{ flexDirection: "row", marginHorizontal: 15 }}>
                    <Icon
                        source={Images.search}
                        size={18}
                        imgStyle={{ marginTop: 30 }} />
                    <TextInput style={styles.input} placeholder='Search for your favourite track' placeholderTextColor='white'></TextInput>
                </View>

            </View>

            <View style={{ flexDirection: "row", marginTop: 20, justifyContent: "space-between", }}>

                <Typography color='white' size={16}>Popular Geners</Typography>
                <Press onPress={()=>{navigation.navigate('Notification')}}>
                    <Typography color='#d93f27' size={14}>View All</Typography>
                </Press>

            </View>

            <View >

                <FlatList
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    data={list}
                    renderItem={({ item, index }) => (

                        <Press scaleSize={0.98} style={{ flex: 1, marginTop: 10 }}>

                            <Image style={{
                                width: 220, height: 150, marginHorizontal: 5, borderRadius: 15,
                            }} source={item.img}></Image>
                            <View style={{ justifyContent: "center", flex: 1, marginLeft: 18, bottom: 8, position: "absolute", }}>
                                <View style={{ flexDirection: "row", borderBottomWidth: 0.2, borderColor: 'white' }}>

                                    <Image style={{ width: 15, height: 15, bottom: 2 }} source={item.img1}></Image>
                                    <Typography color='white' size={13} style={{ marginLeft: 8, bottom: 5 }}>{item.text}</Typography>

                                </View>
                                <Typography color='white' size={12} >{item.text1}</Typography>


                            </View>





                        </Press>

                    )}

                />

            </View>


            <View style={{ flexDirection: "row", marginTop: 20, justifyContent: "space-between", }}>

                <Typography color='white' size={16}>Top Albums</Typography>
                <Press onPress={()=>{navigation.navigate('MyPlayList')}}>
                    <Typography color='#d93f27' size={14}>View All</Typography>
                </Press>

            </View>
            <View >

          <FlatList
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    data={list1}
                    renderItem={({ item, index }) => (

                        <Press scaleSize={0.98} style={{ flex: 1, marginTop: 10 }} 
                        onPress={()=>{navigation.navigate('ViewMusic',{data:item.imgA})}}>

                            <Image style={{
                                width: 110, height: 120, marginHorizontal: 5, borderRadius: 15,
                            }} source={item.imgA}></Image>

                            <View style={{ width: 100,marginTop:5 }}>
                                <Typography
                                    numberOfLines={1}
                                    lineBreakMode="tail"
                                    size={12} style={{ marginLeft: 5 , }}>{item.textA}</Typography>
                             
                            
                             <View style={{ flexDirection: 'row', marginTop:3,width:150,padding:5,bottom:3}}>
                                <Typography color='white' size={12} style={{bottom:1}}>{item.textB}</Typography>
                                <Image style={{ width: 17, height: 16, marginLeft:5 ,}} source={item.imgB}></Image>
                            </View>
                            </View>
                        </Press>



                    )}

                />

            </View>

            <View style={{ flexDirection: "row", marginTop: 20, justifyContent: "space-between", }}>

                <Typography color='white' size={16}>Top Tracks</Typography>
                <Press>
                    <Typography color='#d93f27' size={14}>View All</Typography>
                </Press>

            </View>
            <View >

                <FlatList
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    data={list2}
                    renderItem={({ item, index }) => (

                        <Press scaleSize={0.98} style={{ flex: 1, marginTop: 10 }}
                        onPress={()=>{navigation.navigate('MusicPlayer')}}>

                            <Image style={{
                                width: 110, height: 120, marginHorizontal: 5, borderRadius: 15,
                            }} source={item.imgC}></Image>


                            <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center", marginTop: 5, width: 120, padding: 5 }}>
                                <Icon

                                    source={Images.cart1}
                                    size={15}


                                    imgStyle={{ bottom: 2 }} />

                                <Typography
                                    numberOfLines={1}
                                    lineBreakMode="tail"
                                    color='white' size={13} style={{ marginLeft: 5 }}>{item.textC}</Typography>

                            </View>

                            <View style={{ width: 125 }}>
                                <Typography
                                    numberOfLines={1}
                                    lineBreakMode="tail"
                                    color='white' size={12} style={{ marginLeft: 5 , }}>{item.textD}</Typography>
                             
                            
                             <View style={{ flexDirection: 'row', marginTop:3,width:150,padding:5,bottom:3}}>
                                <Typography size={12} color='white' style={{bottom:1}}>{item.textE}</Typography>
                                <Image style={{ width: 17, height: 16, marginLeft:5 ,}} source={item.imgB}></Image>
                            </View>
                            </View>
                        </Press>
         


                    )}

                />
                        
 
            </View>








        </ScrollContainer>
    )
}
const styles = StyleSheet.create({

    Headers: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: 10

    },
    headerButton: {
        width: 40,
        height: 40,
        backgroundColor: 'black',
        borderWidth: 0.3,
        borderColor: 'white',
        borderRadius: 11,
        justifyContent: 'center',
        alignItems: "center",
        margin: 3


    },
    searchBox: {
        width: '100%',
        height: 50,
        backgroundColor: '#222222',
        alignSelf: "center",
        borderRadius: 10,
        marginTop: 12

    },
    input: {
        width: "98.8%",
        height: 50,
        backgroundColor: '#222222',
        borderRadius: 10,
        paddingLeft: 10,
        fontSize: 12,
        color: 'white',

    }



})

export default Explore