
import { View, Text, Image, ImageBackground, FlatList, StyleSheet } from 'react-native'
import React, { useState } from 'react'
import ScrollContainer from '../../Components/HOC/ScrollContainer'
import AuthHeader from '../../Components/HOC/AuthHeader'
import Typography from '../../Components/UI/Typography'
import Press from '../../Components/HOC/Press'
import images from '../../Constants/images'
import Icon from '../../Components/UI/Icon'

const HomeMusicPR = ({ navigation }) => {
    const [show, setShow] = useState(true)
    const [colorBG, setbgColor] = useState(false)
    const [data, setData] = useState([
        { img: images.Layer1, txt: 'Collectibles' },
        { img: images.Layer3, txt: 'Music' },
        { img: images.Layer5, txt: 'Photography' },
        { img: images.Layer9, txt: 'Collectibles' },
        { img: images.Layer5, txt: 'Photography' },
        { img: images.Layer9, txt: 'Collectibles' },
        { img: images.Layer5, txt: 'Photography' },
        { img: images.Layer9, txt: 'Collectibles' }
    ])
    const [data1, setData1] = useState([
        { img1: images.rockstar, img2: images.user, txt1: '1.50 ETH', },
        { img1: images.rockstar1, img2: images.user, txt1: '1.50 ETH' },
        { img1: images.rockstar2, img2: images.user, txt1: '1.50 ETH' },
        { img1: images.rockstar, img2: images.user, txt1: '1.50 ETH' },
        { img1: images.rockstar2, img2: images.user, txt1: '1.50 ETH' },
        { img1: images.rockstar1, img2: images.user, txt1: '1.50 ETH' },
        { img1: images.rockstar, img2: images.user, txt1: '1.50 ETH' },
        { img1: images.rockstar2, img2: images.user, txt1: '1.50 ETH' }
    ])
    const [dataList, setDatalist] = useState([
        { id: 1, name: "My Total Music ", total: 352 },
        { id: 2, name: "Total Music Sold ", total: '$12352' },
        { id: 3, name: "Total NFT ", total: 52 },
        { id: 4, name: "Total Revenue", total: 352 }
    ])


    const [dataA, setDataA] = useState([


        { imgA: images.Layer1, textB: '1.7million', textC: "Urna Rhoncus Dolor ", textD: 'Neque ,Nisi' },
        { imgA: images.Layer3, textB: '2.3million', textC: "Urna Rhoncus Dolor Neque ,Nisi" },
        { imgA: images.Layer5, textB: '3.7million', textC: "Urna Rhoncus Dolor Neque ,Nisi" },
        { imgA: images.Layer9, textB: '1.7million', textC: "Urna Rhoncus Dolor Neque ,Nisi" },
        { imgA: images.Layer1, textB: '4.7million', textC: "Urna Rhoncus Dolor Neque ,Nisi" },
    ])

    const [dataB, setDataB] = useState([

        {
            txt: "Posted By", txtA: ' (Shourya sh.12)', txtB: ' 3m ago', imagA: images.imgA, txtC: "Romantic of Tortor in ulput",
            txtD: 'By Cam Cam', time: "03.15", likes: '1207', music: '12098', cart: "2345"
        },
        {
            txt: "Posted By", txtA: ' (Shourya sh.12)', txtB: ' 3m ago', imagA: images.imgA, txtC: "Romantic of Tortor in ulput",
            txtD: 'By Cam Cam', time: "03.15", likes: '1207', music: '12098', cart: "2345"
        },
        {
            txt: "Posted By", txtA: ' (Shourya sh.12)', txtB: ' 3m ago', imagA: images.imgA, txtC: "Romantic of Tortor in ulput",
            txtD: 'By Cam Cam', time: "03.15", likes: '1207', music: '12098', cart: "2345"
        },
        {
            txt: "Posted By", txtA: ' (Shourya sh.12)', txtB: ' 3m ago', imagA: images.imgA, txtC: "Romantic of Tortor in ulput",
            txtD: 'By Cam Cam', time: "03.15", likes: '1207', music: '12098', cart: "2345"
        }

    ])

    const [artistData, setArtistData] = useState([

        { ArtistImage: images.Layer1, name: 'Emlifia' },
        { ArtistImage: images.Layer1, name: 'Emlifia' },
        { ArtistImage: images.Layer1, name: 'Emlifia' },
        { ArtistImage: images.Layer1, name: 'Emlifia' },
        { ArtistImage: images.Layer1, name: 'Emlifia' },
        { ArtistImage: images.Layer1, name: 'Emlifia' },
        { ArtistImage: images.Layer1, name: 'Emlifia' }
    ])

    return (
        <ScrollContainer withBG bottomView bottomButton={false} >

            <AuthHeader

                title="Hi Shaurya"
                filterIcon
                shopIcon
            />
            <Typography size={12}>A great day starts with Best music</Typography>


            <View style={{ marginTop: 10 }}>
                <Typography style={{ color: "white", fontSize: 13 }}>Stats</Typography>
                <FlatList
                    data={dataList}
                    numColumns={2}
                    contentContainerStyle={{ margin: 5 }}
                    columnWrapperStyle={{ justifyContent: 'space-between' }}
                    renderItem={({ item, index }) => (

                        <Press style={styles.flatlistStyle1} onPress={()=> navigation.navigate("ChooseMusic")} >
                            <Typography size={12} style={{ color: "red", textAlign: "center" }}>{item.name}</Typography>

                            <Typography size={12} style={{ color: "white", textAlign: "center" }}>{item.total}</Typography>

                        </Press>

                    )}

                />
            </View>

            <Typography size={14} style={{ marginLeft: 5, margintop: 10 }}>Artist</Typography>

            <FlatList
                data={artistData}
                horizontal
                renderItem={({ item, index }) => (
                    <Press style={{ flex: 1 }} onPress={()=>{navigation.navigate('Artistdetails')}}>
                        <View style={styles.mainView}>

                            <Image style={{ width: 70, height: 70, borderRadius: 10 }} source={item.ArtistImage}></Image>


                        </View>
                        <Typography size={12} style={{ textAlign: "center", marginTop: 5 }}>{item.name}</Typography>

                    </Press>
                )}

            />




            <View style={{ flexDirection: "row", marginTop: 10, justifyContent: "space-between" }}>

                <Press style={{
                    width: '45%', padding: 12, backgroundColor: '#222222',
                    borderRadius: 10, alignItems: "center", borderColor: colorBG == true ? 'red' : 'transparent', borderWidth: 1,
                }}
                    onPress={() => {
                        setbgColor(true)
                        setShow(false)
                    }}>

                    <Typography size={14} style={{ color: colorBG == true ? 'red' : 'white' }}>Music</Typography>

                </Press>

                <Press onPress={() => {
                    setbgColor(false)
                    setShow(true)
                }}
                    style={{ width: '48%', padding: 12, backgroundColor: '#222222', borderRadius: 10, alignItems: "center", borderColor: colorBG == true ? 'transparent' : 'red', borderWidth: 1, }}>

                    <Typography size={14} style={{ color: colorBG == true ? 'white' : 'red', }}>NFT</Typography>

                </Press>

            </View>


            {show ?
                <View>
                    <Typography color='#903126' size={15} type='bold' style={{ marginTop: 15 }}>My Collection</Typography>


                    <FlatList
                        data={data}
                        horizontal={true}
                        renderItem={({ item, index }) => (

                            <Press disable scaleSize={0.9} onPress={()=>{navigation.navigate('ViewMusic')}} >

                                <ImageBackground style={{ width: 100, height: 100, margin: 5, flexDirection: "column-reverse" }}
                                    imageStyle={{ borderRadius: 10 }} source={item.img}>


                                    <Typography size={13} style={{ textAlign: 'center', top: 5 }}>{item.txt}</Typography>

                                </ImageBackground>
                            </Press>

                        )}

                    />


                    <Typography color='#903126' type='bold' size={15}style={{ marginTop: 15 }}> NFT</Typography>

                    <FlatList
                        data={data1}
                        columnWrapperStyle={{ justifyContent: "space-around" }}
                        // keyExtractor={item => "#" + item?.id}
                        numColumns={2}
                        renderItem={({ item, index }) => (

                            <Press style={styles.flalist3} onPress={()=>{navigation.navigate('Soon')}}>

                                <ImageBackground style={{ width: '100%', height: 100, alignSelf: "center", flexDirection: 'column-reverse' }}
                                    source={item.img1} imageStyle={{ borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>


                                    <View style={{ flexDirection: "row", marginLeft: 10, top: 5 }}>
                                        <Image style={{ width: 20, height: 20, borderRadius: 10, }} source={item.img2}></Image>
                                        <Typography color='white' size={12} style={{ marginLeft: 5 }}>cooltown</Typography>
                                    </View>


                                </ImageBackground>

                                <View style={{ flexDirection: "row", justifyContent: "space-between", paddingHorizontal: 8, marginTop: 10 }}>
                                    <Typography size={11} color='#903126'>Snoop Dog-B.O.D.R</Typography>
                                    <Icon
                                        size={14}
                                        source={images.heart}
                                    />
                                </View>
                                <Typography size={11} style={{ marginLeft: 8 }}>Music</Typography>

                                <View style={{ flexDirection: "row", justifyContent: "space-between", paddingHorizontal: 8, }}>
                                    <Typography size={11}>Bad Price :</Typography>
                                    <Typography size={11} color='#903126'>{item.txt1}</Typography>
                                </View>

                            </Press>

                        )}
                    />
                </View>





                :



                <View>

                    <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 15, paddingHorizontal: 5 }}>

                        <Typography color='white' size={13}>My Albums</Typography>
                        <Press onPress={()=>{navigation.navigate('MyPlayList')}}>
                            <Typography color='#d93f27' size={12}>View All</Typography>
                        </Press>
                    </View>


                    <FlatList
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        data={dataA}
                        renderItem={({ item, index }) => (

                            <Press onPress={() => navigation.navigate("ViewMusic",{data:item.imgA})} scaleSize={0.9} style={{ width: 120, marginRight: 0, marginTop: 10, }}>

                                <View style={{ flex: 1, borderRadius: 20 }}>
                                    <ImageBackground style={{
                                        width: 110, height: 110, borderRadius: 15,

                                    }} source={item.imgA} imageStyle={{ borderRadius: 10 }} >

                                        <View style={{ flexDirection: "row", justifyContent: "space-between", paddingHorizontal: 5 }}>

                                            <Typography size={10}>{item.textB}</Typography>

                                            <Image style={{ width: 18, height: 18, alignSelf: "flex-end", right: 10, marginTop: 10 }} source={require('../../../Assets/images/heart.png')}></Image>

                                        </View>

                                        <View style={{ alignItems: "center", justifyContent: "center", flex: 1, top: 40 }}>
                                            <Image style={{ width: 16, height: 16, }} source={require('../../../Assets/images/headphone.png')}></Image>

                                        </View>
                                    </ImageBackground>



                                    <View style={{ padding: 10, }}>
                                        <Typography size={10} style={{}}>{item.textC}</Typography>
                                        <Typography size={10} style={{}}>{item.textD}</Typography>
                                    </View>
                                </View>



                            </Press>



                        )}

                    />


                    <View style={{ flexDirection: "row", justifyContent: "space-between", }}>

                        <Typography color='white' size={13}>My Tracks</Typography>
                        <Press >
                            <Typography color='#d93f27' size={12}>View All</Typography>
                        </Press>

                    </View>


                    <View>


                        <FlatList

                            data={dataB}
                            renderItem={({ item, index }) => (

                                <View>
                                    <View style={{ flexDirection: "row", marginTop: 10 }}>
                                        <Typography size={11}>{item.txt}</Typography>
                                        <Typography size={11} color='#d93f27'>{item.txtA}</Typography>
                                        <Typography size={11}>{item.txtB}</Typography>
                                        <View style={{
                                            borderWidth: 0.2, borderColor: "grey", width: '40%', height: 0, marginTop: 12,
                                            marginLeft: 10
                                        }}></View>
                                    </View>


                                    <View style={{ margin: 5 }}>
                                        <Press style={{ flex: 1, marginTop: 10, flexDirection: "row", }}
                                        onPress={()=>{navigation.navigate('MusicPlayer')}}>

                                            <View style={{ borderWidth: 0.5, borderColor: "white", width: 86, height: 86, borderRadius: 10 }}>
                                                <Image style={{ width: 85, height: 85, borderRadius: 10 }} source={item.imagA}></Image>

                                            </View>

                                            <View style={{ flex: 1 }}>
                                                <View style={{ width: 158, marginTop: 10, marginLeft: 10 }}>
                                                    <Typography
                                                        numberOfLines={1}
                                                        lineBreakMode="tail"
                                                        color='white' size={12} style={{ textAlign: "center" }}>{item.txtC}</Typography>
                                                </View>

                                                <View style={{ flexDirection: 'row', justifyContent: "space-between", paddingHorizontal: 12, marginTop: 5 }}>

                                                    <Typography size={11} color='#d93f27' >{item.txtD}</Typography>
                                                    <View style={{ flexDirection: "row", }}>

                                                        <Image style={{ width: 13, height: 13, right: 3 }} source={require('../../../Assets/images/clock.png')}></Image>
                                                        <Typography size={11} style={{}}>{item.time}</Typography>
                                                    </View>



                                                </View>

                                                <View style={{
                                                    borderWidth: 0.2, borderColor: "grey", width: '90%'
                                                    , alignSelf: "center", marginTop: 5
                                                }}></View>

                                                <View style={{ flexDirection: "row", justifyContent: "space-between", paddingHorizontal: 10, marginTop: 10 }}>

                                                    <View style={{ flexDirection: "row" }}>
                                                        <Image style={{ width: 14, height: 14 }} source={require('../../../Assets/images/heart.png')}></Image>
                                                        <Typography size={12} style={{ marginLeft: 5 }}>{item.likes}</Typography>
                                                    </View>

                                                    <View style={{ flexDirection: "row" }}>
                                                        <Image style={{ width: 14, height: 14, top: 2 }} source={require('../../../Assets/images/music.png')}></Image>
                                                        <Typography size={13} style={{ marginLeft: 5, }}>{item.music}</Typography>
                                                    </View>

                                                    <View style={{ flexDirection: "row" }}>
                                                        <Image style={{ width: 14, height: 14 }} source={require('../../../Assets/images/cart.png')}></Image>
                                                        <Typography size={12} style={{ marginLeft: 5 }}>{item.cart}</Typography>
                                                    </View>

                                                </View>

                                            </View>

                                        </Press>



                                    </View>

                                </View>


                            )}
                        />


                    </View>


                </View>


            }




            {/* <View style={{ height: 100 }}>
            </View> */}



        </ScrollContainer>
    )
}


const styles = StyleSheet.create({

    flatlistStyle1: {
        width: '48%',
        height: 70,
        backgroundColor: "#0a0a0a",
        borderRadius: 8,
        alignItems: "center",
        marginTop: 10,
        justifyContent: "center",
        borderColor: 'grey',
        borderWidth: 0.3

    },

    mainView: {
        width: 90,
        height: 90,
        borderColor: "#d93f27",
        borderWidth: 0.5,
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center",
        flex: 1, marginLeft: 8,
        marginTop: 5

    },

    flalist3: {
        width: '48%',
        height: 180,
        backgroundColor: "#222222",
        borderRadius: 10,
        marginTop: 15

    }




})
export default HomeMusicPR