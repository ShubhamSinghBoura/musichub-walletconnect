import { View, Text, StyleSheet, FlatList, Image } from 'react-native'
import React, { useState } from 'react'
import ScrollContainer from '../../Components/HOC/ScrollContainer'
import AuthHeader from '../../Components/HOC/AuthHeader'
import Icon from '../../Components/UI/Icon'
import Press from '../../Components/HOC/Press'
import images from '../../Constants/images'
import Typography from '../../Components/UI/Typography'


const Notification = ({ navigation }) => {
    const [data, setData] = useState([
        { Img: images.Layer3, txt: "Snoop Dog Album Added In Your Favourites", txt1: '8:06 PM' },
        { Img: images.Layer7, txt: "The Love of Life song has been downloaded", txt1: '10:06 PM' },
    ])
    const [data1, setData1] = useState([
        { Img1: images.Layer1, txtA: "Umrao Jaan Album Added In Your Favourites", txtB: '8:06 PM' },
        { Img1: images.Layer5, txtA: "Let's party song has been downloaded", txtB: '10:06 PM' },
        { Img1: images.pic3, txtA: "Let's party song has been downloaded", txtB: '10:06 PM' },
        { Img1: images.pic2, txtA: "Let's party song has been downloaded", txtB: '10:06 PM' },
    ])
    return (

        <ScrollContainer withBG>


            <AuthHeader
                title='Notification'
                back
                shopIcon
                onBackPress={() => { navigation.goBack() }}
            />


            <Typography size={18} style={{ marginTop: 20 }}>Today</Typography>

            <FlatList
                data={data}
                renderItem={({ item, index }) => (

                    <Press style={{ width: '100%', padding: 10, backgroundColor: "#222222", borderRadius: 15, marginTop: 15,
                    }}>

                        <View style={{ flexDirection: "row" }}>

                            <Image style={{ width: 80, height: 80, borderRadius: 10 }} source={item.Img} ></Image>

                            <View style={{ flex: 1,justifyContent:'space-between' }}>
                                <Typography numberOfLines={2} size={13} lineBreakMode="tail"
                                    style={{ marginLeft: 20 }}>{item.txt}</Typography>
                        <Typography size={13} style={{ alignSelf: 'flex-end', marginRight: 5 }}> {item.txt1}</Typography>
                            </View>

                        </View>
                  </Press>

                )}
            />

            <Typography size={18} style={{ marginTop: 20 }}>Yesterday</Typography>
            <FlatList
                data={data1}
                renderItem={({ item, index }) => (
                    <Press style={styles.dataList}>
                        <View style={{ flexDirection: "row" }}>

                            <Image style={{ width: 80, height: 80, borderRadius: 10 }} source={item.Img1} ></Image>

                            <View style={{ flex: 1 ,justifyContent:'space-between'}}>
                                <Typography numberOfLines={2} size={13} lineBreakMode="tail"
                                    style={{ marginLeft: 20 }}>{item.txtA}</Typography>
                        <Typography size={13} style={{ alignSelf: 'flex-end', marginRight: 5, }}> {item.txtB}</Typography>
                            </View>
                        </View>

                    </Press>

                )}
            />
            <View style={{height:100}}></View>
        </ScrollContainer>
    )
}

const styles = StyleSheet.create({

    dataList: {

        width: '100%', 
        padding: 10,
        backgroundColor: "#222222",
        borderRadius: 15,
        marginTop: 15 
    }


})

export default Notification;