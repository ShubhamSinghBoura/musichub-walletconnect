import { StyleSheet, Text, View, Dimensions } from 'react-native'
import React, { useState } from 'react'
import ViewContainer from '../../Components/HOC/ViewContainer'
import ScrollContainer from '../../Components/HOC/ScrollContainer'
import Typography from '../../Components/UI/Typography'
import colors from '../../Constants/colors'
import Press from '../../Components/HOC/Press'
import Swiper from 'react-native-swiper';
const { width, height } = Dimensions.get("window")

const ChooseMusic = ({ navigation }) => {

    const [boxColor, setBoxColor] = useState(true);

    // const SelectColor = () => {
    //     setBoxColor[boxColor]
    // }

    return (
        <ViewContainer>
            <ScrollContainer withBG>

                <View style={{ marginTop: 45, marginBottom: 15 }} >
                    <Typography type="bold" size={18} >
                        Choose the {'\n'}
                        style of music you like
                    </Typography>
                </View>

                <Swiper showsButtons={false}
                    dot={<View style={styles.dot}
                    />}
                    activeDot={<View style={styles.activeDot}
                    />}
                >

                    <View>

                        <View style={{ flexDirection: "row", justifyContent: 'space-between', marginTop: 35 }} >
                            <View>
                                <View style={styles.box1} />
                                <View style={styles.box1} />
                            </View>
                            <View>
                                <Press style={[styles.boxText,]}   >
                                    <Typography type="medium" size={18} color={colors.white} > 1st Orchestra </Typography>
                                </Press>
                            </View>

                            <View style={{ marginTop: -35, justifyContent: "flex-end" }} >
                                <View style={[styles.box1, {
                                    width: width / 7.5,
                                    height: height * 0.06, marginBottom: 50,
                                }]} textAlign="center" />
                                <Press style={[styles.boxText, { width: width / 4.5, height: height * 0.10, }]}  >
                                    <Typography type="medium" size={18} color={colors.white} textAlign="center" > Soul </Typography>
                                </Press>
                            </View>
                        </View>

                        <View style={{ flexDirection: "row", justifyContent: 'space-between', alignItems: "flex-end", marginTop: 25, paddingHorizontal: 5 }} >
                            <View style={{}} >
                                <View style={[styles.box1, {
                                    width: width / 7.5,
                                    height: height * 0.06, alignSelf: "flex-end",
                                }]} />
                                <Press style={[styles.boxText, { width: width / 4.5, height: height * 0.10, borderColor: colors.buttonTextSecondary, }]}  >
                                    <Typography type="medium" size={18} color={colors.white} textAlign="center"  > Metal </Typography>
                                </Press>
                            </View>
                            <View>
                                <Press style={[styles.boxText, { borderColor: colors.buttonTextSecondary, width: 140, height: 140 }]} >
                                    <Typography type="medium" size={18} color={colors.white} textAlign="center" > Classical music </Typography>
                                </Press>
                            </View>
                            <View>
                                <View style={[styles.box1, {
                                    width: width / 7.5,
                                    height: height * 0.06,
                                }]} />
                                <View style={[styles.box1, { marginBottom: 0, }]} />
                            </View>
                        </View>

                        <View style={{ flexDirection: "row", justifyContent: 'space-between', marginTop: 25, paddingHorizontal: 15 }} >
                            <View>
                                <Press style={[styles.boxText, { width: width / 2.2, height: height * 0.22, }]}   >
                                    <Typography type="medium" size={30} color={colors.white} > Rock music </Typography>
                                </Press>
                            </View>

                            <View >
                                <Press style={[styles.boxText, { width: width / 4.5, height: height * 0.10, }]}  >
                                    <Typography type="medium" size={18} color={colors.white} textAlign="center"  > R&B </Typography>
                                </Press>
                                <Press style={[styles.boxText, { width: width / 3.5, height: height * 0.13, marginTop: 15, borderColor: colors.buttonTextSecondary, }]}  >
                                    <Typography type="medium" size={18} color={colors.white} textAlign="center" > Folk </Typography>
                                </Press>
                            </View>
                        </View>

                        <View style={{ flexDirection: "row", justifyContent: 'space-between', marginTop: 5, paddingHorizontal: 15, }} >

                            <View>
                                <Press style={[styles.boxText, { borderColor: colors.buttonTextSecondary, width: 115, height: 115 }]} >
                                    <Typography type="medium" size={18} color={colors.white} > Hip Pop </Typography>
                                </Press>
                            </View>

                            <View style={{ flexDirection: "row", justifyContent: "space-evenly", flex: 1, alignSelf: "flex-end", }} >
                                <Press style={[styles.boxText, { width: width / 4.5, height: height * 0.11, borderColor: colors.buttonTextSecondary }]}  >
                                    <Typography type="medium" size={18} color={colors.white} textAlign="center" > Other </Typography>
                                </Press>
                                <View style={[styles.box1, {
                                    width: width / 7.5,
                                    height: height * 0.06,
                                }]} textAlign="center" />
                            </View>

                        </View>
                    </View>




                    <View>

                        <View style={{ flexDirection: "row", justifyContent: 'space-between', marginTop: 35 }} >
                            <View>
                                <View style={styles.box1} />
                                <View style={styles.box1} />
                            </View>
                            <View>
                                <Press style={[styles.boxText,]}   >
                                    <Typography type="medium" size={18} color={colors.white} > 1st Orchestra </Typography>
                                </Press>
                            </View>

                            <View style={{ marginTop: -35, justifyContent: "flex-end" }} >
                                <View style={[styles.box1, {
                                    width: width / 7.5,
                                    height: height * 0.06, marginBottom: 50,
                                }]} textAlign="center" />
                                <Press style={[styles.boxText, { width: width / 4.5, height: height * 0.10, }]}  >
                                    <Typography type="medium" size={18} color={colors.white} textAlign="center" > Soul </Typography>
                                </Press>
                            </View>
                        </View>

                        <View style={{ flexDirection: "row", justifyContent: 'space-between', alignItems: "flex-end", marginTop: 25, paddingHorizontal: 5 }} >
                            <View style={{}} >
                                <View style={[styles.box1, {
                                    width: width / 7.5,
                                    height: height * 0.06, alignSelf: "flex-end",
                                }]} />
                                <Press style={[styles.boxText, { width: width / 4.5, height: height * 0.10, borderColor: colors.buttonTextSecondary, }]}  >
                                    <Typography type="medium" size={18} color={colors.white} textAlign="center"  > Metal </Typography>
                                </Press>
                            </View>
                            <View>
                                <Press style={[styles.boxText, { borderColor: colors.buttonTextSecondary, width: 140, height: 140 }]} >
                                    <Typography type="medium" size={18} color={colors.white} textAlign="center" > Classical music </Typography>
                                </Press>
                            </View>
                            <View>
                                <View style={[styles.box1, {
                                    width: width / 7.5,
                                    height: height * 0.06,
                                }]} />
                                <View style={[styles.box1, { marginBottom: 0, }]} />
                            </View>
                        </View>

                        <View style={{ flexDirection: "row", justifyContent: 'space-between', marginTop: 25, paddingHorizontal: 15 }} >
                            <View>
                                <Press style={[styles.boxText, { width: width / 2.2, height: height * 0.22, }]}   >
                                    <Typography type="medium" size={30} color={colors.white} > Rock music </Typography>
                                </Press>
                            </View>

                            <View >
                                <Press style={[styles.boxText, { width: width / 4.5, height: height * 0.10, }]}  >
                                    <Typography type="medium" size={18} color={colors.white} textAlign="center"  > R&B </Typography>
                                </Press>
                                <Press style={[styles.boxText, { width: width / 3.5, height: height * 0.13, marginTop: 15, borderColor: colors.buttonTextSecondary, }]}  >
                                    <Typography type="medium" size={18} color={colors.white} textAlign="center" > Folk </Typography>
                                </Press>
                            </View>
                        </View>

                        <View style={{ flexDirection: "row", justifyContent: 'space-between', marginTop: 5, paddingHorizontal: 15, }} >

                            <View>
                                <Press style={[styles.boxText, { borderColor: colors.buttonTextSecondary, width: 115, height: 115 }]} >
                                    <Typography type="medium" size={18} color={colors.white} > Hip Pop </Typography>
                                </Press>
                            </View>

                            <View style={{ flexDirection: "row", justifyContent: "space-evenly", flex: 1, alignSelf: "flex-end", }} >
                                <Press style={[styles.boxText, { width: width / 4.5, height: height * 0.11, borderColor: colors.buttonTextSecondary }]}  >
                                    <Typography type="medium" size={18} color={colors.white} textAlign="center" > Other </Typography>
                                </Press>
                                <View style={[styles.box1, {
                                    width: width / 7.5,
                                    height: height * 0.06,
                                }]} textAlign="center" />
                            </View>

                        </View>
                    </View>


                    {/* <View>

                        <View style={{ flexDirection: "row", justifyContent: 'space-between', marginTop: 35 }} >
                            <View>
                                <View style={styles.box1} />
                                <View style={styles.box1} />
                            </View>
                            <View>
                                <Press style={[styles.boxText,]}   >
                                    <Typography type="medium" size={18} color={colors.white} > 1st Orchestra </Typography>
                                </Press>
                            </View>

                            <View style={{ marginTop: -35, justifyContent: "flex-end" }} >
                                <View style={[styles.box1, {
                                    width: width / 7.5,
                                    height: height * 0.08,
                                }]} textAlign="center" />
                                <Press style={[styles.boxText, { width: 100, height: 100, }]}  >
                                    <Typography type="medium" size={18} color={colors.white} textAlign="center" > Soul </Typography>
                                </Press>
                            </View>
                        </View>

                        <View style={{ flexDirection: "row", justifyContent: 'space-between', alignItems: "flex-end", marginTop: 25, paddingHorizontal: 5 }} >
                            <View style={{}} >
                                <View style={[styles.box1, {
                                    width: width / 7.5,
                                    height: height * 0.08, alignSelf: "flex-end",
                                }]} />
                                <Press style={[styles.boxText, { width: 90, height: 90, borderColor: colors.buttonTextSecondary, }]}  >
                                    <Typography type="medium" size={18} color={colors.white} textAlign="center"  > Metal </Typography>
                                </Press>
                            </View>
                            <View>
                                <Press style={[styles.boxText, { borderColor: colors.buttonTextSecondary, width: 140, height: 140 }]} >
                                    <Typography type="medium" size={18} color={colors.white} textAlign="center" > Classical music </Typography>
                                </Press>
                            </View>
                            <View>
                                <View style={[styles.box1, {
                                    width: width / 7.5,
                                    height: height * 0.08,
                                }]} />
                                <View style={[styles.box1, { marginBottom: 0, }]} />
                            </View>
                        </View>

                        <View style={{ flexDirection: "row", justifyContent: 'space-between', marginTop: 25, paddingHorizontal: 15 }} >
                            <View>
                                <Press style={[styles.boxText, { width: 170, height: 170 }]}   >
                                    <Typography type="medium" size={30} color={colors.white} > Rock music </Typography>
                                </Press>
                            </View>

                            <View >
                                <Press style={[styles.boxText, { width: 85, height: 85, }]}  >
                                    <Typography type="medium" size={18} color={colors.white} textAlign="center"  > R&B </Typography>
                                </Press>
                                <Press style={[styles.boxText, { width: 100, height: 90, marginTop: 15, borderColor: colors.buttonTextSecondary, }]}  >
                                    <Typography type="medium" size={18} color={colors.white} textAlign="center" > Folk </Typography>
                                </Press>
                            </View>
                        </View>

                        <View style={{ flexDirection: "row", justifyContent: 'space-between', marginTop: 5, paddingHorizontal: 15, }} >

                            <View>
                                <Press style={[styles.boxText, { borderColor: colors.buttonTextSecondary, width: 115, height: 115 }]} >
                                    <Typography type="medium" size={18} color={colors.white} > Hip Pop </Typography>
                                </Press>
                            </View>

                            <View style={{ flexDirection: "row", justifyContent: "space-evenly", flex: 1, alignSelf: "flex-end", }} >
                                <Press style={[styles.boxText, { width: 90, height: 90, borderColor: colors.buttonTextSecondary }]}  >
                                    <Typography type="medium" size={18} color={colors.white} textAlign="center" > Other </Typography>
                                </Press>
                                <View style={[styles.box1, {
                                    width: width / 7.5,
                                    height: height * 0.08,
                                }]} textAlign="center" />
                            </View>

                        </View>
                    </View> */}

                </Swiper>






                <View style={{ height: 100 }} ></View>

            </ScrollContainer>
        </ViewContainer>
    )
}

export default ChooseMusic

const styles = StyleSheet.create({

    box1: {
        width: width / 6.5,
        height: height * 0.07,
        borderRadius: 5,
        backgroundColor: colors.buttonTextSecondary,
        marginBottom: 20,
        borderWidth: 0.7,
        borderColor: "#616161",
        marginLeft: 5
    },
    boxText: {
        width: width / 2.7,
        height: height * 0.18,
        // height: 125,
        borderRadius: 5,
        backgroundColor: colors.secondary,
        justifyContent: "center",
        alignItems: "center",
        // borderColor: colors.origin,
        // borderWidth: 1,
        marginLeft: 5,
    },



    dot: {
        backgroundColor: colors.origin,
        width: 10,
        height: 10,
        borderRadius: 50,
        // marginLeft: -120,
        // marginRight: 130,
        borderColor: colors.white,
        // top: 0
    },
    activeDot: {
        backgroundColor: colors.white,
        width: 50,
        height: 10,
        borderRadius: 0,
        marginHorizontal: 5,
        // marginLeft: -120,
        // marginRight: 130,
        borderColor: colors.white,
        // top: 0
    },




})