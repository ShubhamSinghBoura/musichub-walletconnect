import { StyleSheet, Text, View, FlatList, Image ,ImageBackground} from 'react-native'
import React, { useState } from 'react'
import ViewContainer from '../../../Components/HOC/ViewContainer';
import ScrollContainer from '../../../Components/HOC/ScrollContainer';
import AuthHeader from '../../../Components/HOC/AuthHeader';
import Typography from '../../../Components/UI/Typography';
import colors from '../../../Constants/colors';
import Press from '../../../Components/HOC/Press';
import images from '../../../Constants/images';
import GlobalDropDown from '../../../Components/UI/DroupDown/GlobalDrown.android';

const Producer = ({ navigation }) => {

  const [dataCollection, setDataCollection] = useState([
    { img: images.Layer1, txt: 'Collectibles' },
    { img: images.Layer3, txt: 'Music' },
    { img: images.Layer5, txt: 'Photography' },
    { img: images.Layer9, txt: 'Collectibles' },
    { img: images.Layer5, txt: 'Photography' },
    { img: images.Layer9, txt: 'Collectibles' },
    { img: images.Layer5, txt: 'Photography' },
    { img: images.Layer9, txt: 'Collectibles' }
  ])
  const [musicType, setMusicType] = useState([1, 2, 3, 4])
  const [PrState, setPrState] = useState('')
  const [PrStateLAble, setPrStateLAble] = useState('Select PR')
  return (
    <ViewContainer>
      <ScrollContainer
        withBG
        bottomView
        bottomButton={false}
      >

        <AuthHeader
          title="Hi, Shaurya"
          filterIcon
          shopIcon
          onBackPress={() => { }}
          
        />


        <View style={{ marginTop: 20 }} >
          <Typography size={15} type="bold" color={colors.origin} style={{ marginBottom: 10 }} > My Status </Typography>

          <View style={styles.baxBG}>

            <View style={styles.baxBGInner} >
              <Press >
                <Typography size={15} type="regular" textAlign="center" color={colors.origin} >
                  Total Music
                </Typography>
                <Typography size={15} type="bold" textAlign="center" color={colors.white}  >
                  352
                </Typography>
              </Press>

              <View style={{ backgroundColor: colors.white, height: 50 ,width:1.5}} />

              <Press >
                <Typography size={15} type="regular" textAlign="center" color={colors.origin} >
                  Total Music
                </Typography>
                <Typography size={15} type="bold" textAlign="center" color={colors.white}  >
                  352
                </Typography>
              </Press>
            </View>

            <View style={[styles.baxBGInner, { borderBottomWidth: 0 }]} >
              <Press >
                <Typography size={15} type="regular" textAlign="center" color={colors.origin} >
                  Total Music
                </Typography>
                <Typography size={15} type="bold" textAlign="center" color={colors.white}  >
                  352
                </Typography>
              </Press>

              <View style={{ backgroundColor: colors.white, height: 50 ,width:1.5}} />

              <Press >
                <Typography size={15} type="regular" textAlign="center" color={colors.origin} >
                  Total Music
                </Typography>
                <Typography size={15} type="bold" textAlign="center" color={colors.white}  >
                  352
                </Typography>
              </Press>
            </View>
          </View>
        </View>


        <View style={{ marginTop: 20 }} >
          <Typography size={15} type="bold" color={colors.origin} style={{ marginBottom: 0 }} > My PR Status </Typography>

          <View style={styles.statusProfile}>
            <View style={styles.statusProfileBorder} >
              <Image style={styles.Proimg} source={images.Layer1} />
            </View>
            <View style={{ flex: 1, }} >
              <GlobalDropDown
              
                categroy={[
                  { label: 'Emillia', value: 'emillia' },
                  { label: 'Emilliaaa', value: 'emilliaaa' },
                  { label: 'Emilliaaaaaa', value: 'emilliaaaa' }
                ]}
                type={'Select Category'}
                placeholder={PrStateLAble}
                callBack={(v,lable) => {
                  setPrState(v)
                  setPrStateLAble(lable)
                }}
                style={{ height: 55, marginTop: 0 }}
              />
            </View>
          </View>

        </View>



        <View style={[styles.baxBG,{paddingHorizontal:0,}]}>

          <View style={[styles.baxBGInner, { borderBottomWidth: 0, }]} >
            <Press >
              <Typography size={15} type="regular" textAlign="center" color={colors.origin} >
                Total Music
              </Typography>
              <Typography size={15} type="bold" textAlign="center" color={colors.white}  >
                352
              </Typography>
            </Press>

            <View style={{ backgroundColor: colors.white, height: 50 ,width:1.5}} />

            <Press >
              <Typography size={15} type="regular" textAlign="center" color={colors.origin} >
                Total Music
              </Typography>
              <Typography size={15} type="bold" textAlign="center" color={colors.white}  >
                352
              </Typography>
            </Press>
            <View style={{ backgroundColor: colors.white, height: 50 ,width:1.5}} />

            <Press >
              <Typography size={15} type="regular" textAlign="center" color={colors.origin} >
                Total Music
              </Typography>
              <Typography size={15} type="bold" textAlign="center" color={colors.white}  >
                352
              </Typography>
            </Press>
          </View>


        </View>


        <Typography type='bold' color={colors.redBorderColor}  size={15} style={{ marginTop: 15 }}>My Music</Typography>

        <FlatList
            data={dataCollection}
            horizontal={true}
            renderItem={({ item, index }) => (

              <Press onPress={()=>{navigation.navigate('MyPlayList')}}>

                <Image style={{ width: 100,borderRadius: 10, height: 100, margin: 5 }} source={item.img}/>
                <Typography color={colors.redBorderColor} textAlign='center' size={10} >5 Track</Typography>
                <Typography color={colors.white} textAlign='center' size={10} >Raybeats</Typography>
              </Press>


            )}

          />


<Typography type='bold' color={colors.redBorderColor} size={15} style={{ marginTop: 15 }}>My NFT</Typography>

<FlatList
    data={dataCollection}
    horizontal={true}
    renderItem={({ item, index }) => (

      <Press onPress={()=>{navigation.navigate('NFTS')}} >

        <ImageBackground style={{ width: 100, height: 100, margin: 5, flexDirection: "column-reverse" }}
          imageStyle={{ borderRadius: 10 }} source={item.img}>


          <Typography size={13} style={{ textAlign: 'center', top: 5 }}>{item.txt}</Typography>

        </ImageBackground>

      </Press>


    )}

  />










      </ScrollContainer>
    </ViewContainer>
  )
}

export default Producer;

const styles = StyleSheet.create({
  baxBG: {
    backgroundColor: colors.bgColor,
    borderWidth: 1,
    borderColor: colors.buttonTextSecondary,
    flex: 1,
    // width: "100%",
    justifyContent: "center",
    alignItems: "center",
    // paddingVertical: 20,
    borderRadius: 10,
    paddingHorizontal: 20,



  },



  baxBGInner: {
    flexDirection: "row",
    justifyContent: 'space-between',
    flex: 1,
    width: "100%",
    // justifyContent: "space-between",
    alignItems: "center",
    borderBottomColor: colors.white,
    borderBottomWidth: 1,
    // borderRightColor: colors.white,
    // borderRightWidth: 1,
    // marginHorizontal:10,
    // marginTop:20,
    //  bottom:20,
    padding: 20,

  },

  statusProfile: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  Proimg: {
    width: 45,
    height: 45,
    borderRadius: 10,
  },
  statusProfileBorder: {
    borderColor: colors.origin,
    borderRadius: 10,
    borderWidth: 1,
    padding: 5,
    marginRight: 10,
    width: 55,
    height: 55,
  },




})