import { View, Text, StyleSheet, FlatList, Image } from 'react-native'
import React, { useState } from 'react'
import ScrollContainer from '../../Components/HOC/ScrollContainer'
import AuthHeader from '../../Components/HOC/AuthHeader'
import Icon from '../../Components/UI/Icon'
import images from '../../Constants/images'
import Typography from '../../Components/UI/Typography'
import Press from '../../Components/HOC/Press'


const myAccount = ({ navigation }) => {
    const [data, setData] = useState([
        { img: images.userIcon, txt: "Edit Profile", img1: images.leftArrow },
        { img: images.passwordA, txt: "Change Password", img1: images.leftArrow },


    ])

    const [data1, setData1] = useState([
        { imgA: images.playlist, txtA: "My Playlist", img1: images.leftArrow , screens:'MyPlayList' },
        { imgA: images.album, txtA: "Album", img1: images.leftArrow ,screens:'Soon'},
        { imgA: images.fav, txtA: "Favorites", img1: images.leftArrow ,screens:'Fevorites'},
        { imgA: images.artist, txtA: "Artist", img1: images.leftArrow,screens:'Artist' },
        { imgA: images.downloads, txtA: "Downloads", img1: images.leftArrow,screens:'Soon' },


    ])
    return (

        <ScrollContainer withBG bottomView  bottomButton={false}>
            <AuthHeader
                back
                title={'My Account'}
                shopIcon
                onBackPress={() => { navigation.goBack() }}
                style
            />
            <View style={styles.main}>
                <View style={{ flexDirection: "row" }}>
                    <Icon
                        source={images.Layer1}
                        size={80}
                    />
                    <View style={{ width: '70%', marginLeft: 15, marginTop: 5 }}>
                        <Typography numberOfLines={1} lineBreakMode='tail' size={15}>Shaurya Sharma</Typography>
                        <Typography numberOfLines={1} lineBreakMode='tail' size={13}>dummy1234@gmail.com</Typography>
                    </View>
                </View>
                <View style={styles.threeButton}>
                    <Press scaleSize={1.05} style={styles.pressStyle}>
                        <View>
                            <Typography
                                type='semiBold'
                                size={15}
                                style={{
                                    alignSelf: 'center', color: 'white'
                                }}>03K </Typography>
                            <View>
                                <Typography type='semiBold'
                                    size={15}>Followers</Typography>
                            </View>
                        </View>
                    </Press>
                    <View style={{width:2,height:40,backgroundColor:"white",marginTop:3}}></View>

                    <Press scaleSize={1.05}  style={styles.pressWidth}>
                        <Typography type='semiBold' size={15} style={{ alignSelf: 'center', color: 'white' }}>177</Typography>
                        <View>
                            <View>
                                <Typography type='semiBold' size={13}>Tracks</Typography>
                            </View>
                        </View>
                    </Press>
                    <View style={{width:2,height:40,backgroundColor:"white",marginTop:3}}></View>
                    <Press
                        scaleSize={1.05} onPress={() => { }} style={styles.pressWidth}>
                        <Typography
                            type='medium' size={15} style={{ alignSelf: 'center', color: 'white' }}>3.2M</Typography>
                        <View>
                            <View>
                                <Typography type='semiBold' size={13}>Plays</Typography>
                            </View>
                        </View>
                    </Press>
                </View>
            </View>
            <Typography size={16} type='bold' style={{ marginTop: 10 }}>Account</Typography>
            <View style={styles.flatlisMainview}>
                <FlatList
                    data={data}
                    renderItem={({ item, index }) => (
                        <View style={{ width: '100%',  }}>
                            <Press style={styles.flatlistPress} scaleSize={0.99} onPress={()=>{navigation.navigate('Soon')}}>
                                <View style={{ flexDirection: "row" }}>
                                    <Image style={styles.icons} source={item.img}></Image>
                                    <Typography size={13} style={{ marginLeft: 10, marginTop: 3 }}>
                                        {item.txt}
                                    </Typography>
                                </View>
                                <Icon
                                    size={13}
                                    source={images.leftArrow}
                                    imgStyle={{ marginTop: 10 }} />
                            </Press>
                            {!(index==data.length-1)&&<View style={styles.viewLine}></View>}
                        </View>
                    )}
                />
            </View>
            <Typography size={16} type='bold' style={{ marginTop: 10 }}>Activity</Typography>
            <View style={styles.flatlisMainview}>
                <FlatList
                    data={data1}
                    renderItem={({ item, index }) => (
                        <View style={{ width: '100%',justifyContent:'center' }}>
                            <Press scaleSize={0.99} style={styles.flatlistPress} onPress={()=>{navigation.navigate(item.screens)}}>
                                <View style={{ flexDirection: "row" }}>
                                    <Image style={styles.icons} source={item.imgA}></Image>
                                    <Typography size={13} style={{ marginLeft: 10 }}>{item.txtA}</Typography>
                                </View>
                                <Icon
                                    size={13}
                                    source={images.leftArrow}/>
                            </Press>
                            {!(index==data1.length-1)&&<View style={styles.viewLine}></View>}
                        </View>
                    )}
                />
            </View>

          

        </ScrollContainer>

    )
}

const styles = StyleSheet.create({

    main: {

        width: '100%',
        padding: 20,
        backgroundColor: "#222222",
        marginTop: 20,
        borderRadius: 10,
      

    },
    threeButton: {

        flexDirection: 'row',
        paddingVertical: 15,
        flex: 1,
       

    },
    pressStyle: {

        flex: 1,
        backgroundColor: '#222222',
        justifyContent: 'center',
        alignItems: 'center'
    },
    pressWidth: {

        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
       
    },
    flatlisMainview: {

        backgroundColor: "#222222",
        flex: 1,
        borderRadius: 10,
        marginTop: 10

    },
    flatlistPress: {
        height:55,
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: 8,
        alignItems:'center'

    },
    icons: {
        width: 16,
        height: 16,
        tintColor: "white",
        resizeMode: 'contain',
        // marginTop: 3
    },

    viewLine: {
        width: '95%',
        backgroundColor: 'white',
        height: 1,
        alignSelf: "center"
    }


})

export default myAccount