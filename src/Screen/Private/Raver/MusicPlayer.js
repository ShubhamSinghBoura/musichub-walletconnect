import { View, Text, StyleSheet, ImageBackground, FlatList, Image } from 'react-native'
import React, { useState, useEffect } from 'react'
import ScrollContainer from '../../../Components/HOC/ScrollContainer'
import AuthHeader from '../../../Components/HOC/AuthHeader'
import Icon from '../../../Components/UI/Icon'
import images from '../../../Constants/images'
import Typography from '../../../Components/UI/Typography'
import Press from '../../../Components/HOC/Press'
import { useIsFocused } from "@react-navigation/native";
import colors from '../../../Constants/colors'



const MusicPlayer = ({ navigation }) => {
const [playMusic, setplayMusic] = useState(false)
    const [show, setShow] = useState('track')
    const [dataB, setDataB] = useState([

        {
            imagA: images.imgA, txtC: "Romantic of Tortor in ulput",
            txtD: 'By Cam Cam', time: "03.15", likes: '1207', music: '12098', cart: "2345"
        },
        {
            imagA: images.imgA, txtC: "Romantic of Tortor in ulput",
            txtD: 'By Cam Cam', time: "03.15", likes: '1207', music: '12098', cart: "2345"
        },
        {
            imagA: images.imgA, txtC: "Romantic of Tortor in ulput",
            txtD: 'By Cam Cam', time: "03.15", likes: '1207', music: '12098', cart: "2345"
        },
        {
            imagA: images.imgA, txtC: "Romantic of Tortor in ulput",
            txtD: 'By Cam Cam', time: "03.15", likes: '1207', music: '12098', cart: "2345"
        }

    ])

    const [data1, setData1] = useState([
        { Img1: images.Layer1, txtA: "DLD.Wi", txtA1: 'Bid 7 minutes ago', txtB: '11:57 ETH', txtB1: '$36,089.89' },
        { Img1: images.Layer1, txtA: "DLD.Wi", txtA1: 'Bid 7 minutes ago', txtB: '11:57 ETH', txtB1: '$36,089.89' },
    ])
    const [data, setData] = useState([

        { img: images.Layer1, txt: "DLD.Wi", txt1: "Bid 7 minutes ago", txt2: "11.23ETH", txt3: "$34.588" },
        { img: images.Layer1, txt: "DLD.Wi", txt1: "Bid 7 minutes ago", txt2: "11.23ETH", txt3: "$34.588" },
        { img: images.Layer1, txt: "DLD.Wi", txt1: "Bid 7 minutes ago", txt2: "11.23ETH", txt3: "$34.588" },
    ])
    const [bgColor, setbgColor] = useState(false)

    return (

        <ScrollContainer withBG bottomView bottomButton={false}>

            <AuthHeader
                back
                shopIcon
                onBackPress={() => { navigation.goBack() }}
            />

            <View style={Styles.headerImg}>
                <Icon
                    source={images.pic2}
                    size={175}
                    imgStyle={{ borderRadius: 10, }} />
                <View style={Styles.headerTxtView}>
                    <Typography size={16} >ScoolKing</Typography>
                    <Typography size={14}>Fruit Du Demon (2020)</Typography>
                </View>

            </View>



            <View style={{ flexDirection: "row", alignSelf: "center", padding: 30, }}>
                <Press>
                    <Icon
                        source={images.download}
                        size={25}
                    />
                </Press>
                <Press style={{ marginHorizontal: 35 }}>
                    <Icon
                        source={images.heart}
                        size={25}
                    />
                </Press>
                <Press>
                    <Icon
                        source={images.ellipse1}
                        size={25}
                    />
                </Press>
            </View>

            {/* MusicPlayer */}









            <View style={{ width: "100%", height: 5, backgroundColor: "white", borderRadius: 10, flexDirection: "row", alignItems: "center" }}>


                <View style={{ height: 5, backgroundColor: colors.redBorderColor, width: '40%', borderRadius: 10 }}>



                </View>

                <View style={{ height: 20, width: 20, backgroundColor: colors.white, borderRadius: 10, marginLeft: -20 }}></View>

            </View>



           <View style={{flexDirection:"row",justifyContent:"space-between",marginTop:5}}>


            <Typography size={14}>01:27</Typography>
            <Typography size={14}>4:27</Typography>

           </View>

             
             <View style={{flexDirection:"row",justifyContent:"space-around",alignItems:"center",marginVertical:10}}>


                <Icon
                source={images.shuffle}
                />

               
                <Icon
                source={images.pre}/>
                <Press
                onPress={()=>setplayMusic(!playMusic)}
                style={{
                    width:60,height:60,
                    borderRadius:30,justifyContent:'center',
                    alignItems:'center',backgroundColor:colors.redBorderColor
                }}>
                <Icon
                tintColor={colors.white}
                  source={ !playMusic?images.pause:images.play}
                  size={20}/>    
                </Press>
                
                <Icon
                source={images.nextA}/>
             
                <Icon
                    source={images.repeat}/>
                


             </View>





            <View style={Styles.logoView}>

                <View style={{ flexDirection: "row" }}>

                    <Icon
                        source={images.logoA}
                        size={40}
                        imgStyle={{ borderRadius: 20, resizeMode: "contain" }} />

                    <View style={{ marginTop: 3, marginLeft: 10 }}>

                        <Typography color='#d93f27' size={14}>msgt.music</Typography>
                        <Typography size={12} style={{ marginTop: 3 }}>3176 Followers</Typography>

                    </View>
                </View>
                <Press style={Styles.followButton}>

                    <Typography size={14}>Follow</Typography>

                </Press>

            </View>
            <View style={{ flex: 1 }}>
                <View style={Styles.threeButton}>

                    <Press onPress={() => {

                        setShow('track')
                    }}>
                        <Typography size={15} style={{ color: show == "track" ? '#903126' : 'white' }}>Tracks-25</Typography>
                    </Press>
                    <View style={Styles.singleBorder}></View>
                    <Press onPress={() => {

                        setShow('info')
                    }}>
                        {console.log("mc aaja", show)}
                        <Typography size={15} style={{ color: show == "info" ? '#903126' : 'white' }}>Info</Typography>
                    </Press>
                    <View style={Styles.singleBorder1}></View>
                    <Press onPress={() => {

                        setShow('offer')
                    }}>
                        <Typography size={15} style={{ color: show == "offer" ? '#903126' : 'white' }}>Offers</Typography>
                    </Press>
                </View>



            </View>

            {(show == 'track') ?
                <View style={{ flex: 1 }}>
                    <FlatList

                        data={dataB}
                        renderItem={({ item, index }) => (

                            <View>

                                <View style={{ margin: 5 }}>
                                    <Press style={{ flex: 1, marginTop: 10, flexDirection: "row", }}>

                                        <View style={{ borderWidth: 0.5, borderColor: "white", width: 86, height: 86, borderRadius: 10 }}>
                                            <Image style={{ width: 85, height: 85, borderRadius: 10 }} source={item.imagA}></Image>

                                        </View>

                                        <View style={{ flex: 1 }}>
                                            <View style={{ width: 158, marginTop: 10, marginLeft: 10 }}>
                                                <Typography
                                                    numberOfLines={1}
                                                    lineBreakMode="tail"
                                                    color='white' size={12} style={{ textAlign: "center" }}>{item.txtC}</Typography>
                                            </View>

                                            <View style={{ flexDirection: 'row', justifyContent: "space-between", paddingHorizontal: 12, marginTop: 5 }}>

                                                <Typography size={11} color='#d93f27' >{item.txtD}</Typography>
                                                <View style={{ flexDirection: "row", }}>

                                                    <Image style={{ width: 13, height: 13, right: 3 }} source={require('../../../../Assets/images/clock.png')}></Image>
                                                    <Typography size={11} style={{}}>{item.time}</Typography>
                                                </View>



                                            </View>

                                            <View style={{
                                                borderWidth: 0.2, borderColor: "grey", width: '90%'
                                                , alignSelf: "center", marginTop: 5, height: 0.8
                                            }}></View>

                                            <View style={{ flexDirection: "row", justifyContent: "space-between", paddingHorizontal: 10, marginTop: 10 }}>

                                                <View style={{ flexDirection: "row" }}>
                                                    <Image style={{ width: 14, height: 14 }} source={require('../../../../Assets/images/heart.png')}></Image>
                                                    <Typography size={12} style={{ marginLeft: 5 }}>{item.likes}</Typography>
                                                </View>

                                                <View style={{ flexDirection: "row" }}>
                                                    <Image style={{ width: 14, height: 14, top: 2 }} source={require('../../../../Assets/images/music.png')}></Image>
                                                    <Typography size={13} style={{ marginLeft: 5, }}>{item.music}</Typography>
                                                </View>

                                                <View style={{ flexDirection: "row" }}>
                                                    <Image style={{ width: 14, height: 14 }} source={require('../../../../Assets/images/cart.png')}></Image>
                                                    <Typography size={12} style={{ marginLeft: 5 }}>{item.cart}</Typography>
                                                </View>

                                            </View>

                                        </View>

                                    </Press>



                                </View>

                            </View>


                        )}
                    />
                </View>
                : (show == "info") ?

                    <View style={{ alignItems: "center", marginTop: 20, }}>
                        <Typography size={14} style={{ textAlign: 'justify' }}>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                            took a galley of type and scrambled it to make a type specimen book.
                            <Press ><Typography size={14} type='medium' color='#d93f27' style={{}}> Show more</Typography></Press></Typography>

                    </View>

                    : (show == "offer") ?
                        <View style={{ flex: 1, marginTop: 20 }}>

                            <FlatList
                                data={data}
                                renderItem={({ item, index }) => (


                                    <Press style={{ backgroundColor: "#222222", flex: 1, height: 90, borderRadius: 10, paddingHorizontal: 15, marginBottom: 10 }}>

                                        <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 12 }}>
                                            <View style={{ flexDirection: "row" }}>

                                                <Image style={{ width: 70, height: 70, borderRadius: 10 }} source={item.img}></Image>
                                                <View style={{ marginLeft: 15, marginTop: 5 }}>
                                                    <Typography size={13}>{item.txt}</Typography>
                                                    <Typography size={12} style={{ marginTop: 10 }}>{item.txt1}</Typography>
                                                </View>
                                            </View>
                                            <View style={{ marginTop: 10 }}>
                                                <Typography color='#d93f27' size={14}>{item.txt2}</Typography>
                                                <Typography size={14}>{item.txt3}</Typography>


                                            </View>

                                        </View>


                                    </Press>

                                )}
                            />


                        </View>
                        :
                        null
            }

            {/* <View style={{ height: 100 }}></View> */}
        </ScrollContainer>


    )
}

const Styles = StyleSheet.create({

    headerImg: {

        flex: 1,
        backgroundColor: 'transparent',
        alignItems: "center",
        marginTop: 20
    },

    headerTxtView: {

        flex: 1,
        alignItems: 'center',
        marginTop: 10

    },

    logoView: {
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    followButton: {
        padding: 10,
        minWidth: 80,
        alignItems: "center",
        borderRadius: 10,
        justifyContent: "center",
        borderColor: "#d93f27",
        borderWidth: 0.7

    },

    threeButton: {

        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingHorizontal: 25,
        width: "100%",
        height: 50,
        backgroundColor: "#222222",
        borderRadius: 10,
        marginTop: 25

    },
    singleBorder: {

        width: 1,
        backgroundColor: "#d93f27",
        height: 30,

    },
    singleBorder1: {

        width: 1,
        backgroundColor: "#d93f27",
        height: 30,

    },
    dataList: {

        width: '100%',
        height: 80,
        backgroundColor: "#222222",
        borderRadius: 15,
        marginTop: 15,

    }





})

export default MusicPlayer





