import { View, Text, TextInput, Image, StyleSheet,FlatList } from 'react-native'
import React,{useState} from 'react'
import AuthHeader from '../../Components/HOC/AuthHeader'
import ScrollContainer from '../../Components/HOC/ScrollContainer'
import Icon from '../../Components/UI/Icon'
import images from '../../Constants/images'
import Typography from '../../Components/UI/Typography'
import Images from '../../Constants/images'
import Press from '../../Components/HOC/Press';
const Transaction = ({navigation}) => {

    const [dataList,setdataList] = useState([
        { img:images.pic2 , text:"New NFT Genrated" , text1:"$25.00" , text2:"16 july 2022 8:06 PM"},
        { img:images.pic1 , text:"New Track Purchased" , text1:"$65.00" , text2:"18 july 2022 10:06 PM"},
        { img:images.pic3 , text:"Paid To Methew" , text1:"$15.00" , text2:"12 july 2022 9:06 PM"},
        { img:images.pic2 , text:"Recived From Mike" , text1:"$25.00" , text2:"12 july 2022 8:06 PM"},
        { img:images.pic3 , text:"New Track Downloaded" , text1:"$55.00" , text2:"13 july 2022 4:06 PM"},
        { img:images.pic1 , text:"New NFT genrated" , text1:"$35.00" , text2:"14 july 2022 5:06 PM"},
        { img:images.pic3 , text:"New NFT genrated" , text1:"$45.00" , text2:"19 july 2022 7:06 PM"}
    ])
    return (

        <ScrollContainer withBG bottomView bottomButton={false}>
            <AuthHeader
                back
                title={'Transaction'}
                shopIcon
                filterIcon
                onBackPress={() => { navigation.goBack() }}
                 />

         

                <View style={{ flexDirection: "row", alignSelf:"center",marginTop:20 }}>

                    <TextInput style={styles.input} placeholder='Search transaction you are looking for...' placeholderTextColor='grey'></TextInput>
                </View>


        <FlatList
         
         data={dataList}
         renderItem={({ item, index }) => (

               <Press style={styles.viewBox}>
              
              <View style={styles.viewStyle}>

                  <View style={{flex:1}}>

                  <Typography size={13}>{item.text}</Typography>
                  <Typography color='#d93f27' size={13}>{item.text1}</Typography>
                  <Typography size={13}>{item.text2}</Typography>

                  </View>
                 
                 <Image style={{width:60,height:60,borderRadius:10}} source={item.img}></Image>

              </View>


               </Press>

         )}
         
        />

           {/* <View style={{height:0}}></View> */}
        </ScrollContainer>
    )
}

const styles = StyleSheet.create({

    viewBox:{

        width:'100%' , 
        padding:10,
        backgroundColor:"#222222",
        borderRadius:10,
        marginTop:15

    },

    viewStyle:{

        flexDirection:"row" ,
        justifyContent:"space-between",paddingHorizontal:5
    },
  
    input: {
        width: "100%",
         padding:10,
        backgroundColor: '#222222',
        borderRadius: 10,
        paddingLeft: 20,
        fontSize: 14,
        color: 'white',
    },


})

export default Transaction