import { StyleSheet, Image, View, FlatList } from 'react-native'
import React from 'react'
import ScrollContainer from '../../Components/HOC/ScrollContainer';
import AuthHeader from '../../Components/HOC/AuthHeader';
import { useState } from 'react';
import images from '../../Constants/images';
import Typography from '../../Components/UI/Typography';
import colors from '../../Constants/colors';
import { TouchableOpacity } from 'react-native-gesture-handler';

const Favorites = ({navigation}) => {

  const [fav, setFav] = useState([1, 2, 3, 4, 5, 6])

  return (
    <ScrollContainer
      withBG
      bottomButton={false}
     
      bottomView
    >
      <AuthHeader
        title='Favorites'
        back
       
        onBackPress={() => { navigation.goBack('') }}
      />

      <View style={{ marginTop: 15 }} >

      </View>
      <FlatList
        data={fav}
        renderItem={({ item, index }) => {
          return (
            <>
              <View style={styles.boxRow} >
             
                <View>
                  <Image style={styles.Img} source={images.img1} />
                </View>


                <View style={{ ...styles.boxRow, marginTop: 0, borderBottomColor: "white", borderBottomWidth: 1, marginLeft: 15 }} >
                  <View style={{ justifyContent: "flex-start", flex: 1, marginRight: 7 }} >
                    <Typography color={colors.white} size={13} type="medium" > Romatic of tortor in ulput... </Typography>
                    <Typography color={colors.origin} size={12} type="regulr" style={{ marginVertical: 3, }} > by CamCam </Typography>
                    <Typography color={colors.white} size={11} type="medium"> $10.00 </Typography>
                  </View>

                 
                  <View style={{ justifyContent: 'space-between', height: 65,paddingVertical:10 }}  >
                  <TouchableOpacity style={{alignSelf: 'center'}}   >
                 
                  <Image style={styles.Icon} source={images.ellipse}   />
                 
                  </TouchableOpacity>
                   
                    <TouchableOpacity style={{ backgroundColor: colors.inputBox,padding: 5,borderRadius: 5 }} >
                      <Image style={styles.Icon} source={images.cart1} />
                    </TouchableOpacity>
                  </View>


                </View>



              </View>

            </>

          )
        }}


      />
    </ScrollContainer>

  )
}

export default Favorites;

const styles = StyleSheet.create({
  boxRow: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginTop: 15,
    height: 65
    //  backgroundColor:"green"
  },
  Img: {
    width: 65,
    height: 65,
    borderRadius: 5,
  },
  Icon: {
    width: 15,
    height: 15,
    // marginVertical: 9,

  }


})