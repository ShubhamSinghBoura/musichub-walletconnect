import { View, Text, StyleSheet, TouchableOpacity, FlatList, Image, ImageBackground } from 'react-native'
import React, { useState } from 'react'
import ScrollContainer from '../../Components/HOC/ScrollContainer';
import AuthHeader from '../../Components/HOC/AuthHeader';
import Icon from '../../Components/UI/Icon';
import images from '../../Constants/images';
import Typography from '../../Components/UI/Typography';
import Press from '../../Components/HOC/Press';


const Artistdetails = ({ navigation }) => {
    const [data, setData] = useState([

        { img: images.Layer1, text: '$9.00', text1: 'Romantic of tortornnn' },
        { img: images.Layer3, text: '$91.00', text1: 'Romantic of tortormm' },
        { img: images.Layer5, text: '$43.00', text1: 'Romantic of tortormm' },
        { img: images.Layer7, text: '$45.00', text1: 'Romantic of tortormm' },
        { img: images.Layer9, text: '$19.00', text1: 'Romantic of tortormm' },

    ])

    const [data1, setData1] = useState([


        { img1: images.Layer1, textA: 'Urna Rhoncus' },
        { img1: images.Layer3, textA: 'Urna Rhoncus' },
        { img1: images.Layer5, textA: 'Urna Rhoncus' },
        { img1: images.Layer9, textA: 'Urna Rhoncus' },
        { img1: images.Layer1, textA: 'Urna Rhoncus' },
    ])

    return (
        <ScrollContainer withBG bottomView bottomButton={false}>

            <AuthHeader
                back
                onBackPress={()=>{navigation.goBack()}}
            />

            <View style={{ flex: 1, alignItems: "center", marginTop: 30 }}>
                <Icon
                    size={150}
                    source={images.Layer5}
                    imgStyle={{ borderRadius: 15 }} />

                <Typography size={20} type='semibold' style={{ marginTop: 10 }}>Eric Allbright</Typography>
                <Typography size={15} type='semibold' style={{ marginTop: 5 }}>Loriem ipsum dolor sit amet,consecetetur</Typography>
            </View>




            <View style={{
                width: '100%',
                backgroundColor: '#222222',
                alignSelf: 'center',
                borderRadius: 10,
                marginTop: 30,

            }}>

                <View style={{
                    flexDirection: 'row',
                    paddingVertical: 10,
                    flex: 1,
                }}>

                    <Press

                        scaleSize={1.1}
                        style={{
                            flex: 1,
                            backgroundColor: '#222222',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                        <Typography
                            type='semiBold'
                            size={16}
                            style={{
                                alignSelf: 'center', color: 'white'
                            }}>34.5 K</Typography>

                        <Typography
                            type='semiBold'
                            size={12}
                            style={{
                                alignSelf: 'center', color: 'white'
                            }}>Followers</Typography>
                    </Press>
                    <View style={{width:2,height:35,backgroundColor:"white",marginTop:3}}></View>
                    <Press
                        scaleSize={1.1}


                        style={{
                            flex: 1,
                          
                        }}>
                        <Typography
                            type='medium'
                            size={16}
                            style={{ alignSelf: 'center', color: 'white' }}>177</Typography>
                        <Typography
                            type='medium'
                            size={12}
                            style={{ alignSelf: 'center', color: 'white' }}>Tracks</Typography>
                    </Press>
                    <View style={{width:2,height:35,backgroundColor:"white",marginTop:3}}></View>
                    <Press
                        scaleSize={1.1}


                        style={{
                            flex: 1,
                           
                        }} >
                        <Typography
                            type='medium'
                            size={16}
                            style={{ alignSelf: 'center', color: 'white' }}>3.2m</Typography>
                        <Typography
                            type='medium'
                            size={12}
                            style={{ alignSelf: 'center', color: 'white' }}>plays</Typography>
                    </Press>


                </View>




            </View>

            <View style={{ flexDirection: "row", marginTop: 30, justifyContent: "space-between" }}>

                <Press style={{ width: '45%', padding: 12, backgroundColor: '#222222', borderRadius: 10, alignItems: "center" }}>

                    <Typography size={14}>Follows</Typography>

                </Press>
                <Press style={{ width: '48%', padding: 12, backgroundColor: '#222222', borderRadius: 10, alignItems: "center" }}>

                    <Typography size={14} >Message</Typography>

                </Press>

            </View>

            <View style={{ alignItems: "center", marginTop: 20 }}>
                <Typography size={12} style={{ textAlign: 'justify' }}>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                    took a galley of type and scrambled it to make a type specimen book.
                    <Press ><Typography size={12} type='medium' color='#d93f27' style={{ top: 5 }}> Show more</Typography></Press></Typography>
            </View>

            <View style={{ flexDirection: "row", marginTop: 30, justifyContent: "space-between", }}>

                <Typography color='white' size={16}>Tracks</Typography>
                <Press >
                    <Typography color='#d93f27' size={14}>View All</Typography>
                </Press>
            </View>

            <View>
                <FlatList
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    data={data}
                    renderItem={({ item, index }) => (

                        <Press  style={{ flex: 1, marginTop: 10 }}>

                            <View style={{ flex: 1, }}>
                                <ImageBackground style={{
                                    width: 110, height: 110, marginHorizontal: 5,

                                }} imageStyle={{ borderRadius: 10 }} source={item.img}>

                                    <Image style={{ width: 18, height: 18, alignSelf: "flex-end", right: 10, marginTop: 10 }} source={require('../../../Assets/images/heart.png')}></Image>

                                    <View style={{ alignItems: "center", justifyContent: "center", flex: 1, top: 40 }}>
                                        <Image style={{ width: 16, height: 16, }} source={require('../../../Assets/images/headphone.png')}></Image>

                                    </View>
                                </ImageBackground>


                                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center", marginTop: 5, width: 120, padding: 5 }}>
                                    <Icon

                                        source={images.cart1}
                                        size={12}


                                        imgStyle={{ bottom: 2 }} />

                                    <Typography
                                        numberOfLines={1}
                                        lineBreakMode="tail"
                                        color='white' size={13} style={{ marginLeft: 5 }}>{item.text}</Typography>

                                </View>

                                <View style={{ width: 125 }}>
                                    <Typography
                                        numberOfLines={1}
                                        lineBreakMode="tail"
                                        color='white' size={12} style={{ marginLeft: 5, }}>{item.text1}</Typography>



                                </View>
                            </View>
                        </Press>



                    )}

                />
            </View>


            <View style={{ flexDirection: "row", marginTop: 30, justifyContent: "space-between", }}>

                <Typography color='white' size={16}>Albums</Typography>
                <Press >
                    <Typography color='#d93f27' size={14}>View All</Typography>
                </Press>

            </View>


            <View>

                <FlatList
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    data={data1}
                    renderItem={({ item, index }) => (

                        <Press  style={{ flex: 1, marginTop: 10 }}>

                            <View style={{ flex: 1, borderRadius: 20 }}>
                                <ImageBackground style={{
                                    width: 110, height: 110, marginHorizontal: 5, borderRadius: 15,

                                }} source={item.img1} imageStyle={{ borderRadius: 10 }} >

                                    <View style={{ flexDirection: "row", justifyContent: "space-between", paddingHorizontal: 5 }}>

                                        <Typography size={10}>{item.txt}</Typography>

                                        <Image style={{ width: 18, height: 18, alignSelf: "flex-end", right: 10, marginTop: 10 }} source={require('../../../Assets/images/heart.png')}></Image>

                                    </View>

                                    <View style={{ alignItems: "center", justifyContent: "center", flex: 1, top: 40 }}>
                                        <Image style={{ width: 16, height: 16, }} source={require('../../../Assets/images/headphone.png')}></Image>

                                    </View>
                                </ImageBackground>


                                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center", marginTop: 5, width: 120, padding: 5, }}>

                                </View>


                                <Typography
                                    numberOfLines={1}
                                    lineBreakMode="tail"
                                    color='white' size={13} style={{ textAlign: "center" }}>{item.textA}</Typography>
                            </View>
                        </Press>



                    )}

                />


            </View>


          
        </ScrollContainer>
    )
}

const styles = StyleSheet.create({



})
export default Artistdetails