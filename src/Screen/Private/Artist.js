import { View, Text, StyleSheet, Image, FlatList } from 'react-native'
import React, { useState } from 'react'
import ScrollContainer from '../../Components/HOC/ScrollContainer'
import AuthHeader from '../../Components/HOC/AuthHeader'
import Images from '../../Constants/images'
import images from '../../Constants/images'
import Typography from '../../Components/UI/Typography'
import Press from '../../Components/HOC/Press'

const Artist = ({navigation}) => {
  const [list, setList] = useState([
    { img: images.pic1, text: 'Roc Legeon', img1: images.check },
    { img: images.pic2, text: 'Josh Petru', img1: images.check },
    { img: images.pic3, text: 'Kabeazy', img1: images.check },
    { img: images.Layer1, text: 'Raybeats', img1: images.check },
    { img: images.Layer3, text: 'Paven Melody', img1: images.check },
    { img: images.Layer7, text: 'Young Taylor', img1: images.check },
    { img: images.pic3, text: 'Ayy Walker', img1: images.check },
    { img: images.Layer9, text: 'Palaze', img1: images.check },
    { img: images.pic2, text: 'Deemarc', img1: images.check },
    { img: images.pic1, text: 'Kid Ocean', img1: images.check },
    { img: images.pic3, text: 'Next Beats', img1: images.check },
    { img: images.Layer3, text: 'Kyle Junior', img1: images.check },
    { img: images.pic1, text: 'Roc Legeon', img1: images.check },
    { img: images.pic2, text: 'Josh Petru', img1: images.check },
    { img: images.pic3, text: 'Kabeazy', img1: images.check },

  ])
  return (

    <ScrollContainer withBG
      bottomView
      bottomButton={false}>

      <AuthHeader
        back
        title={'Artists'}
        shopIcon

        onBackPress={() => { navigation.goBack() }}
        style />

      <FlatList

        data={list}
        numColumns={3}
        renderItem={({ item, index }) => (

          <Press style={styles.mainView} onPress={()=>{navigation.navigate('Artistdetails')}}>

            <Image style={styles.TotalImages } source={item.img}></Image>

            <View style={{ flexDirection: 'row', margin: 10 }}>
              <Typography numberOfLines={1} size={13} >{item.text}</Typography>
              <Image style={styles.checkImage} source={item.img1}></Image>
            </View>

          </Press>

        )}
      />


    </ScrollContainer>

  )
}
const styles = StyleSheet.create({

  mainView: {
    flex: 1,
    alignItems: "center",
    marginTop: 20,

  },

  TotalImages:{
    width: 100, 
    height: 100, 
    borderRadius: 50,
  },
  checkImage:{
    height: 15,
    width: 15, 
    borderRadius: 10, 
    marginLeft: 5 

  }

})

export default Artist;
