import {Text, View} from 'react-native';
import React, {useState} from 'react';
import ViewContainer from '../../../Components/HOC/ViewContainer';
import Card from '../../../Components/UI/Card';
import Typography from '../../../Components/UI/Typography';
import CommonButton from '../../../Components/UI/CommonButton';
import ButtonRow from '../../../Components/UI/ButtonRow';
import IconButton from '../../../Components/UI/IconButton';
import Icon from '../../../Components/UI/Icon';
import BottomSheet from '../../../Components/UI/BottomSheet/BottomSheet';

const Profile = () => {
  const [ShowSheet, setShowSheet] = useState(false);
  const hideSheet = () => {
    setShowSheet(false);
  };
  return (
    <>
      <ViewContainer backgroundColor="#171717">
        <Card>
          <Typography type="bold" color="#fff" size={18}>
            How to make VS Code autocomplete React component's prop types while
            using the component in JSX markup? P.S.: I'm using JS, not TS.
          </Typography>
          <ButtonRow>
            <CommonButton
              //  halfButton
              text="Read More Ok Done"
              backgroundColor="#fff"
              textColor="#000"
            />
            <IconButton
              backgroundColor="#fff"
              text="On Press"
              textColor="#000"
              onPress={() => {
                setShowSheet(!ShowSheet);
              }}
              source={require('../../../../Assets/images/React-icon.png')}
            />
            <IconButton
              backgroundColor="#fff"
              text="On Press"
              textColor="#000"
              source={require('../../../../Assets/images/React-icon.png')}
            />
            <Icon
              source={require('../../../../Assets/images/React-icon.png')}
            />
          </ButtonRow>
        </Card>
      </ViewContainer>
      {ShowSheet && (
        <BottomSheet
          onClose={hideSheet}
          actions={[
            {
              title: 'Profile',
              onPress: () => {},
            },
            {
              title: 'Settings',
              onPress: () => {},
            },
          ]}
        />
      )}
    </>
  );
};

export default Profile;
