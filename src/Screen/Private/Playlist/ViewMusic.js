import React, {useState} from 'react';
import ScrollContainer from '../../../Components/HOC/ScrollContainer';
import Icon from '../../../Components/UI/Icon';
import Typography from '../../../Components/UI/Typography';
import Card from '../../../Components/UI/Card';
import ProgressBar from '../../../Components/HOC/ProgressBar';
import colors from '../../../Constants/colors';
import {STANDARD_WIDTH} from '../../../Constants/layout';
import {View, FlatList, Image} from 'react-native';
import AuthHeader from '../../../Components/HOC/AuthHeader';
import images from '../../../Constants/images';
import Press from '../../../Components/HOC/Press';

const ViewMusic = ({navigation, route}) => {
  return (
    <ScrollContainer
      withBG
      bottomText="Edit Playlist"
      onBottomPress={() => {}}
      bottomView>
      <AuthHeader
        back
        onBackPress={() => {
          navigation.goBack();
        }}
      />

      <View style={{marginVertical: 20, marginTop: 30}}>
        <Image
          source={route.params?.data}
          style={{
            width: STANDARD_WIDTH * 0.75,
            height: 200,
            borderRadius: 10,
            resizeMode: 'cover',
            alignSelf: 'center',
          }}
        />
        <View style={{marginVertical: 10}}>
          <Typography type="semiBold" size={18} textAlign="center">
            My Playlist
          </Typography>
          <Typography
            textAlign="center"
            size={12}
            type="regular"
            style={{marginTop: 5}}>
            Lorem ipsum dolor sit amet consectetur
          </Typography>
        </View>

        <View
          style={{
            width: STANDARD_WIDTH,
            backgroundColor: colors.inputBox,
            flexDirection: 'row',
            flex: 1,
            height: 60,
            borderRadius: 20,
            paddingVertical: 10,
            marginVertical: 10,
          }}>
          <Press
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            
            }}>
            <Icon size={20} source={images.repeat} />
          </Press>
          <View style={{width:2,height:30,backgroundColor:"white",marginTop:5}}></View>
          <Press
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            
            }}>
            <Icon size={20} source={images.heart} />
          </Press>
          <View style={{width:2,height:30,backgroundColor:"white",marginTop:5}}></View>
          <Press
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            
            }}>
            <Icon size={20} source={images.request} />
          </Press>
          <View style={{width:2,height:30,backgroundColor:"white",marginTop:5}}></View>
          <Press
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon size={20} source={images.share} />
          </Press>
        </View>
        <FlatList
          style={{marginTop: 10}}
          numColumns={2}
          data={[
            {track: 5, name: 'Published', label: '15 July 2022'},
            {track: 5, name: 'Number of Tracks', label: '25'},
            {track: 5, name: 'Reposts', label: '20'},
            {track: 5, name: 'Likes', label: '4562'},
          ]}
          renderItem={({item, index}) => {
            return (
              <View
                style={{
                  width: STANDARD_WIDTH * 0.48,
                  marginRight: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 0,
                  borderWidth: 2,
                  paddingVertical: 10,
                  borderColor: colors.inputBox,
                  borderRadius: 10,
                  marginTop: 10,
                }}>
                <View style={{}}>
                  <Typography
                    color={colors.redBorderColor}
                    size={13}
                    textAlign="center"
                    type="semiBold"
                    style={{marginBottom: 5}}>
                    {item?.name}
                  </Typography>
                  <Typography textAlign="center" size={14} type="semiBold">
                    {item?.label}
                  </Typography>
                </View>
              </View>
            );
          }}
        />
        <View
          style={{
            //   width: STANDARD_WIDTH * 0.48,
            marginRight: 10,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 0,
            borderWidth: 2,
            paddingVertical: 10,
            borderColor: colors.inputBox,
            borderRadius: 10,
            marginTop: 10,
            padding: 10,
          }}>
          <View style={{}}>
            <Typography
              color={colors.redBorderColor}
              size={13}
              type="semiBold"
              style={{marginBottom: 5}}>
              About
            </Typography>
            <Typography size={14} type="medium">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500
            </Typography>
          </View>
        </View>
      </View>
    </ScrollContainer>
  );
};

export default ViewMusic;
