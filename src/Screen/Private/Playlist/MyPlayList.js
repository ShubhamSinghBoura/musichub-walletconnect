import React, {useState} from 'react';
import ScrollContainer from '../../../Components/HOC/ScrollContainer';
import Icon from '../../../Components/UI/Icon';
import Typography from '../../../Components/UI/Typography';
import Card from '../../../Components/UI/Card';
import ProgressBar from '../../../Components/HOC/ProgressBar';
import colors from '../../../Constants/colors';
import {STANDARD_WIDTH} from '../../../Constants/layout';
import {View, FlatList, Image} from 'react-native';
import AuthHeader from '../../../Components/HOC/AuthHeader';
import images from '../../../Constants/images';
import Press from '../../../Components/HOC/Press';

const MyPlayList = ({navigation}) => {
  return (
    <ScrollContainer
      withBG
      bottomText="Create New Playlist"
      onBottomPress={() => {navigation.navigate('Soon')}}
      bottomView>
      <AuthHeader
        title="My Playlist"
        back
        onBackPress={() => {
          navigation.goBack();
        }}
      />

      <View style={{marginVertical: 20, marginTop: 30}}>
        <Typography size={17} type="semiBold">
          {'Created By Me'}
        </Typography>
        <FlatList
          style={{}}
          numColumns={2}
          data={[
            {track: 5, name: 'Rockbeats', image: images.rockstar},
            {track: 10, name: 'Rockbeats', image: images.rockstar1},
            {track: 50, name: 'Rockbeats', image: images.rockstar2},
          ]}
          renderItem={({item, index}) => {
            return (
              <Press
                style={{
                  width: STANDARD_WIDTH / 2,
                  marginRight: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 20,
                }}
                onPress={() => {
                  navigation.navigate('ViewMusic', {data: item.image});
                }}>
                <Image
                  source={item.image}
                  style={{
                    width: STANDARD_WIDTH * 0.45,
                    height: 120,
                    borderRadius: 10,
                    resizeMode: 'cover',
                  }}
                />
                <View style={{marginTop: 10}}>
                  <Typography
                    color={colors.redBorderColor}
                    size={12}
                    textAlign="center"
                    type="semiBold">
                    {item.track} Tracks
                  </Typography>
                  <Typography textAlign="center" size={14} type="semiBold">
                    {item?.name}
                  </Typography>
                </View>
              </Press>
            );
          }}
        />
      </View>
    </ScrollContainer>
  );
};

export default MyPlayList;
