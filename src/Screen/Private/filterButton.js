import { View, Text, TextInput, StyleSheet, FlatList } from 'react-native'
import React, { useState } from 'react'
import ScrollContainer from '../../Components/HOC/ScrollContainer'
import AuthHeader from '../../Components/HOC/AuthHeader'
import Typography from '../../Components/UI/Typography'
import CommonInput from '../../Components/UI/CommonInput'
import Press from '../../Components/HOC/Press'
import fonts from '../../Constants/fonts'
import Icon from '../../Components/UI/Icon'
import images from '../../Constants/images'

const filterButton = ({ navigation }) => {
    const [data, setData] = useState([

        { txt: 'Move' },
        { txt: 'Chill' },
        { txt: 'Relaxing' },
        { txt: 'Angry' },
        { txt: 'Beach Time' },
        { txt: 'Relaxing' },
        { txt: "For the Anxious" },
        { txt: 'Move' },


    ])

    const [data1, setData1] = useState([

        { txt: 'Party' },
        { txt: 'Breakup' },
        { txt: `Valentine's` },
        { txt: 'Angry' },
        { txt: 'Gym' },
        { txt: 'Relaxing' },
        { txt: "For the Anxious" },
        { txt: 'Move' },


    ])
    const [bgColor, setBgColor] = useState(false)
    const [bgColor1, setBgColor1] = useState(false)

    return (

        <ScrollContainer withBG bottomView bottomText={'Filter'}>

            <AuthHeader close />

            <Typography size={20}>Search</Typography>
            <View style={{ flex: 1, marginTop: 15 }}>

                <TextInput style={Styles.input} placeholder='Search for artists and moods'
                    placeholderTextColor='white' ></TextInput>


            </View>
            <Typography type='' size={18} style={{ marginTop: 20 }}>Mood</Typography>


            <View>

                <FlatList

                    numColumns={3}
                    data={data}
                    renderItem={({ item, index }) => (

                        <Press style={{
                            padding: 8, borderWidth: 0.9, borderColor: "#d93f27", borderRadius: 10,
                            minWidth: 100, justifyContent: "center", alignItems: "center", marginTop: 15, marginRight: 10,

                            backgroundColor: index == bgColor ? '#d93f27' : 'transparent'
                        }}
                            onPress={() => { setBgColor(index) }} >

                            <Typography size={14}>{item.txt}</Typography>

                        </Press>

                    )}
                />



            </View>


            {/* second list */}


            <Typography type='' size={18} style={{ marginTop: 20 }}>Occasion</Typography>


            <View>

                <FlatList

                    numColumns={3}
                    data={data1}
                    renderItem={({ item, index }) => (

                        <Press style={{
                            padding: 8, borderWidth: 0.9, borderColor: "#d93f27", borderRadius: 10,
                            minWidth: 100, justifyContent: "center", alignItems: "center", marginTop: 15, marginRight: 10
                            , backgroundColor: index == bgColor1 ? '#d93f27' : 'transparent'
                        }}
                            onPress={() => { setBgColor1(index) }} >
                            <Typography size={14}>{item.txt}</Typography>
                        </Press>
                    )}
              />
            </View>

        


         
      

         

        </ScrollContainer>


    )
}

const Styles = StyleSheet.create({

    input: {

        width: '100%',
        padding: 10,
        backgroundColor: "#3a3a3a",
        borderRadius: 10,
        paddingLeft: 30,
        fontSize: 14,
        color: "white",
        fontFamily: fonts.regular

    },






})

export default filterButton