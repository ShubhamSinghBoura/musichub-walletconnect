import React, {useState} from 'react';
import CommonButton from '../../../Components/UI/CommonButton';
import ButtonRow from '../../../Components/UI/ButtonRow';
import ScrollContainer from '../../../Components/HOC/ScrollContainer';
import Icon from '../../../Components/UI/Icon';
import Typography from '../../../Components/UI/Typography';
import Card from '../../../Components/UI/Card';
import IconButton from '../../../Components/UI/IconButton';
import ProgressBar from '../../../Components/HOC/ProgressBar';
import colors from '../../../Constants/colors';
import {STANDARD_WIDTH} from '../../../Constants/layout';
import {View} from 'react-native';
import Press from '../../../Components/HOC/Press';
import AuthHeader from '../../../Components/HOC/AuthHeader';
import {FlatList} from 'react-native-gesture-handler';
import BlackInput from '../../../Components/UI/TextInput/BlackInput';

const Home = ({navigation}) => {
  const [selected, setSelected] = useState('Fixed Price');
  const [price, setPrice] = useState();
  const [royalties, setRoyalties] = useState();
  return (
    <ScrollContainer
      withBG
      bottomText="Next"
      onBottomPress={() => {
        navigation.navigate('CreateNft1');
      }}
      bottomView>
      <AuthHeader title="Create New NFT" back onBackPress={() => {}} />
      <ProgressBar
        shouldAnimated={false}
        value={2}
        width={STANDARD_WIDTH - 20}
      />
      <View style={{marginVertical: 20}}>
        <Typography size={17} type="semiBold">
          {'Select Sale Type & Price'}
        </Typography>
      </View>

      <FlatList
        data={[
          {text: 'Fixed Price'},
          {text: 'Fixed Auction'},
          {text: 'Limitless Auction'},
        ]}
        contentContainerStyle={{
          justifyContent: 'space-between',
          flexDirection: 'row',
          flexWrap: 'wrap',
        }}
        renderItem={({item, index}) => {
          return (
            <Press
              style={{
                width: (STANDARD_WIDTH - 40) / 3,
                backgroundColor: colors.inputBox,
                padding: 20,
                borderRadius: 10,
                marginRight: 1,
                justifyContent: 'center',
                alignItems: 'center',
                borderWidth: 2,
                borderColor:
                  item?.text == selected
                    ? colors.redBorderColor
                    : colors.inputBox,
                height: '100%',
              }}
              onPress={() => setSelected(item?.text)}
              disable={selected == item?.text}>
              <Icon
                source={require('../../../../Assets/images/cart.png')}
                size={30}
              />
              <Typography
                type="semiBold"
                size={12}
                textAlign="center"
                style={{marginTop: 10}}>
                {item?.text}
              </Typography>
            </Press>
          );
        }}
      />
      <View style={{marginTop: 20}}>
        <BlackInput placeholder="1.3 ETH" label="Your Price" />

        <View style={{flexDirection: 'row', marginTop: 10}}>
          <Typography size={13} type="medium">
            Service Fee:
          </Typography>
          <Typography size={13} type="semiBold" color={colors.redBorderColor}>
            {' '}
            2.5%
          </Typography>
        </View>
        <View style={{flexDirection: 'row', marginTop: 10}}>
          <Typography size={13} type="medium">
            You will Receive:
          </Typography>
          <Typography size={13} type="semiBold" color={colors.redBorderColor}>
            {' '}
            1.323ETH / $4,126.03
          </Typography>
        </View>
        <BlackInput
          placeholder="% you get when the item resold"
          label="Royalties"
          style={{marginVertical: 10}}
        />
        <Typography size={13} type="medium">
          If you don't fill this, the royalties will be automatically{' '}
          <Typography size={13} type="semiBold" color={colors.redBorderColor}>
            {' '}
            10%
          </Typography>
        </Typography>
      </View>
    </ScrollContainer>
  );
};

export default Home;
