
import { StyleSheet, View, ImageBackground, Image, FlatList } from 'react-native'
import React, { useState } from 'react'
import ViewContainer from '../../Components/HOC/ViewContainer'
import images from '../../Constants/images'
import FormContainer from '../../Components/HOC/FormContainer'
import Card from '../../Components/UI/Card'
import AuthHeader from '../../Components/HOC/AuthHeader'
import CommenFileUpload from '../../Components/UI/CommenFileUpload'
import Typography from '../../Components/UI/Typography'
import colors from '../../Constants/colors'
import BlackInput from '../../Components/UI/TextInput/BlackInput'
import fonts from '../../Constants/fonts'
import { STANDARD_WIDTH } from '../../Constants/layout';
import Icon from '../../Components/UI/Icon'
import GlobalDropDown from '../../Components/UI/DroupDown/GlobalDrown';

const CreateTrack = (props) => {
    const [category, setCategory] = useState([]);
    const [categorySelected, setCategorySelected] = useState([]);
    const [categoryLabel, setCategoryLabel] = useState();
    const [modal, setModal] = useState(false);
    const [tags, setTags] = useState([]);

    const textToShow = (arr, placeholder) => {
      
        if (arr.length > 0) {
            return tags.join('');
        } else {
            return placeholder;
        }
        
    };
    return (
        <ViewContainer>
            <ImageBackground source={images.backgroundbg} style={styles.dotsbg}>
                <FormContainer backgroundColor="transparent" 
                withBG 
                bottomText='Next'
                    onBottomPress={()=>{ props.navigation.navigate("Favorites")
            
                }}
                bottomView
                >

                    <View>
                        <AuthHeader
                            title="CreateTrack"
                            back
                            onBackPress={() => { }}
                        />

                        <View style={styles.uploadeVBox} >
                            <Typography type="medium" color={colors.origin} size={15} > Upload Audio Files For Download </Typography>
                            <Typography type="medium" color={colors.white} size={15}  > Choose your file to upload </Typography>
                            <CommenFileUpload />
                        </View>


                        <View style={styles.uploadeVBox} >
                            <View style={{ flex: 1 }} >
                                <Typography type="medium" color={colors.origin} size={15} > Upload New Image </Typography>
                                <Typography type="medium" color={colors.white} size={15}  > Choose image for track cover </Typography>
                            </View>
                            <View style={{ flexDirection: "row", justifyContent: "space-between", }} >

                                <View style={{ flex: 1 }} >
                                    <CommenFileUpload
                                        uploadeBg={{ marginRight: 5, }}
                                        uploadeIconText="Preferred: 1500*1500px"
                                        uploadeIconSize="Minimum: 500*500px"
                                    />
                                </View>
                                <View style={{ flex: 1 }} >
                                    <CommenFileUpload
                                        uploadeIcon={images.photo}
                                        uploadeBg={{ marginLeft: 5, }}
                                        uploadeIconText={false}
                                        uploadeIconSize={false}
                                        
                                    />
                                </View>

                            </View>
                        </View>

                        <View style={styles.uploadeVBox} >
                            <BlackInput
                                label="Title"
                                placeholder="New Track"
                            />
                        </View>


                        <View style={styles.uploadeVBox} >
                            <Typography
                                type="semiBold"
                                color={colors.redBorderColor}
                                size={14}
                                style={{ marginLeft: 10, }}>
                                Tag ({tags.length})
                            </Typography>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                }}>
                                <Card
                                    style={{
                                        paddingHorizontal: 18,
                                        paddingVertical: 15,
                                        width: STANDARD_WIDTH - 70,
                                        alignSelf: 'flex-start',
                                        height: 50,

                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                    }}>
                                    <View style={{ width: STANDARD_WIDTH - 160 }}>
                                        <FlatList
                                            data={tags}
                                            horizontal={true}
                                            style={{}}
                                            contentContainerStyle={{
                                                flexDirection: 'row',
                                            }}
                                            ListEmptyComponent={() => {
                                                return (
                                                    <Typography size={12}  type="bold" style={{}}>
                                                        Select Tags
                                                    </Typography>
                                                );
                                            }}
                                            renderItem={({ item, index }) => {
                                                return (
                                                    <Typography
                                                        size={13}
                                                        type="bold"
                                                        style={{
                                                            marginRight: 10,
                                                            textDecorationLine: 'underline',
                                                            color:
                                                                index % 2 == 0 ? colors.white : colors.redBorderColor,
                                                        }}
                                                        type="semiBold">
                                                        {item}
                                                    </Typography>
                                                );
                                            }}
                                        />
                                    </View>
                                    <Typography size={12} type="bold" >{textToShow(tags, '').length}/25</Typography>
                                </Card>
                                <Card
                                    style={{
                                        width: 50,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        height: 50,
                                    }}
                                    disabled={false}
                                    onPress={() => {
                                        setModal(true);
                                    }}>
                                    <Icon
                                        size={16}
                                        imgStyle={{ resizeMode: 'contain', tintColor:"#fff" }}
                                        source={require('../../../Assets/images/add.png')}
                                    />
                                </Card>
                            </View>

                        </View>


                        <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop:10 }} >
                            <View style={{ flex: 1, marginRight: 5 }}>
                                <GlobalDropDown
                                    categroy={[{ label: 'sdad', value: 'sdasdas' }]}
                                    label={'Track'}
                                    value={''}
                                    type={'Select Category'}
                                    placeholder={'Beat'}
                                    callBack={(v, label) => {
                                        console.log(v, 'asdas');
                                    }}
                                />
                            </View>
                            <View style={{ flex: 1, marginLeft: 5 }}>
                                <GlobalDropDown
                                    categroy={[{ label: 'sdad', value: 'sdasdas' }]}
                                    label={'Release Date'}
                                    value={''}
                                    type={'Select Category'}
                                    placeholder={'15/7/2022'}
                                    callBack={(v, label) => {
                                        console.log(v, 'asdas');
                                    }}

                                />
                            </View>
                        </View>

                        <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop:10 }} >
                            <View style={{ flex: 1, marginRight: 5 }}>
                                <GlobalDropDown
                                    categroy={[{ label: 'sdad', value: 'sdasdas' }]}
                                    label={'Primary Genre'}
                                    value={''}
                                    type={'Select Category'}
                                    placeholder={'HipHop'}
                                    callBack={(v, label) => {
                                        console.log(v, 'asdas');
                                    }}
                                />
                            </View>
                            <View style={{ flex: 1, marginLeft: 5 }}>
                                <GlobalDropDown
                                    categroy={[{ label: 'sdad', value: 'sdasdas' }]}
                                    label={'SubGenre'}
                                    value={''}
                                    type={'Select Category'}
                                    placeholder={'Pop'}
                                    callBack={(v, label) => {
                                        console.log(v, 'asdas');
                                    }}

                                />
                            </View>
                        </View>

                        <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop:10 }} >
                            <View style={{ flex: 1, marginRight: 5 }}>
                                <GlobalDropDown
                                    categroy={[{ label: 'sdad', value: 'sdasdas' }]}
                                    label={'Primary Mood'}
                                    value={''}
                                    type={'Select Category'}
                                    placeholder={'Adored'}
                                    callBack={(v, label) => {
                                        console.log(v, 'asdas');
                                    }}
                                />
                            </View>
                            <View style={{ flex: 1, marginLeft: 5 }}>
                                <GlobalDropDown
                                    categroy={[{ label: 'sdad', value: 'sdasdas' }]}
                                    label={'Secondary Mood'}
                                    value={''}
                                    type={'Select Category'}
                                    placeholder={'Pure'}
                                    callBack={(v, label) => {
                                        console.log(v, 'asdas');
                                    }}

                                />
                            </View>
                        </View>








                        <View style={styles.uploadeVBox} >
                            <BlackInput
                                placeholder="Enter your text here"
                                label="Description"
                                style={{ marginTop: 10 }}
                                inputStyle={{
                                    height: 100,
                                    textAlignVertical: 'top',
                                    fontSize: 13,
                                    fontFamily: fonts.semiBold,
                                }}
                                multiline={true}
                            />
                        </View>


                        <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop:10 }} >
                            <View style={{ flex: 1, marginRight: 5 }}>
                                <GlobalDropDown
                                    categroy={[{ label: 'sdad', value: 'sdasdas' }]}
                                    label={'Beats per minute'}
                                    value={''}
                                    type={'Select Category'}
                                    placeholder={'0.02'}
                                    callBack={(v, label) => {
                                        console.log(v, 'asdas');
                                    }}
                                />
                            </View>
                            <View style={{ flex: 1, marginLeft: 5 }}>
                                <GlobalDropDown
                                    categroy={[{ label: 'sdad', value: 'sdasdas' }]}
                                    label={'Key'}
                                    value={''}
                                    type={'Select Category'}
                                    placeholder={'None'}
                                    callBack={(v, label) => {
                                        console.log(v, 'asdas');
                                    }}
                                />
                            </View>
                        </View>



                    </View>

                </FormContainer>
            </ImageBackground>
        </ViewContainer>
    )
}

export default CreateTrack

const styles = StyleSheet.create({
    uploadeVBox: {
        marginTop: 25,
    },
    dotsbg: {
        flex: 1,
        width: "100%",
    },
})