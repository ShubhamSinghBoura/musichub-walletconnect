import { View, Text, FlatList, Image, ImageBackground, StyleSheet } from 'react-native'
import React, { useState } from 'react'
import ScrollContainer from '../../../Components/HOC/ScrollContainer'
import AuthHeader from '../../../Components/HOC/AuthHeader'
import Typography from '../../../Components/UI/Typography'
import Press from '../../../Components/HOC/Press'
import images from '../../../Constants/images'
import Icon from '../../../Components/UI/Icon'
import { useDrawer } from '../../../Context/UserContext'
import colors from '../../../Constants/colors'

const HomeMusicRaver = ({ navigation }) => {
  const {userDrawer}=useDrawer()
  const [dataB, setDataB] = useState([

    {
      txt: "Posted By", txtA: ' (Shourya sh.12)', txtB: ' 3m ago', imagA: images.imgA, txtC: "Romantic of Tortor in ulput",
      txtD: 'By Cam Cam', time: "03.15", likes: '1207', music: '12098', cart: "2345"
    },
    {
      txt: "Posted By", txtA: ' (Shourya sh.12)', txtB: ' 3m ago', imagA: images.imgA, txtC: "Romantic of Tortor in ulput",
      txtD: 'By Cam Cam', time: "03.15", likes: '1207', music: '12098', cart: "2345"
    },
    {
      txt: "Posted By", txtA: ' (Shourya sh.12)', txtB: ' 3m ago', imagA: images.imgA, txtC: "Romantic of Tortor in ulput",
      txtD: 'By Cam Cam', time: "03.15", likes: '1207', music: '12098', cart: "2345"
    },
    {
      txt: "Posted By", txtA: ' (Shourya sh.12)', txtB: ' 3m ago', imagA: images.imgA, txtC: "Romantic of Tortor in ulput",
      txtD: 'By Cam Cam', time: "03.15", likes: '1207', music: '12098', cart: "2345"
    }

  ])

  const [dataA, setDataA] = useState([


    { imgA: images.Layer1, textB: '1.7million', textC: "Urna Rhoncus Dolor ", textD: 'Neque ,Nisi' },
    { imgA: images.Layer3, textB: '2.3million', textC: "Urna Rhoncus Dolor Neque ,Nisi" },
    { imgA: images.Layer5, textB: '3.7million', textC: "Urna Rhoncus Dolor Neque ,Nisi" },
    { imgA: images.Layer9, textB: '1.7million', textC: "Urna Rhoncus Dolor Neque ,Nisi" },
    { imgA: images.Layer1, textB: '4.7million', textC: "Urna Rhoncus Dolor Neque ,Nisi" },
  ])


  const [data, setData] = useState([
    { imageBack: images.backImage1 },
    { imageBack: images.backImage2 },
    { imageBack: images.backImage1 },
    { imageBack: images.backImage2 },
    { imageBack: images.backImage1 },
    { imageBack: images.backImage2 },


  ])

  const [datas, setDatas] = useState([

    {
      coverImage: images.Layer9, backText: 'Et Platia Adipicing Torotr Sed. Proin Partuient',
      watchTime: "03:10", views: "23345"
    },

    {
      coverImage: images.Layer7, backText: 'Et Platia Adipicing Torotr Sed. Proin Partuient',
      watchTime: "03:10", views: "23345"
    },

    {
      coverImage: images.Layer9, backText: 'Et Platia Adipicing Torotr Sed. Proin Partuient',
      watchTime: "03:10", views: "23345"
    },

    {
      coverImage: images.Layer7, backText: 'Et Platia Adipicing Torotr Sed. Proin Partuient',
      watchTime: "03:10", views: "23345"
    },

    {
      coverImage: images.Layer9, backText: 'Et Platia Adipicing Torotr Sed. Proin Partuient',
      watchTime: "03:10", views: "23345"
    },

    {
      coverImage: images.Layer7, backText: 'Et Platia Adipicing Torotr Sed. Proin Partuient',
      watchTime: "03:10", views: "23345"
    }

  ])

  const [artistData, setArtistData] = useState([

    { artistImage: images.artist1, artistText: "Emilla" },
    { artistImage: images.artist2, artistText: "Emilla" },
    { artistImage: images.artist3, artistText: "Emilla" },
    { artistImage: images.artist4, artistText: "Emilla" },

  ])

  const [dataCollection, setDataCollection] = useState([
    { img: images.Layer1, txt: 'Collectibles' },
    { img: images.Layer3, txt: 'Music' },
    { img: images.Layer5, txt: 'Photography' },
    { img: images.Layer9, txt: 'Collectibles' },
    { img: images.Layer5, txt: 'Photography' },
    { img: images.Layer9, txt: 'Collectibles' },
    { img: images.Layer5, txt: 'Photography' },
    { img: images.Layer9, txt: 'Collectibles' }
  ])

  const [data1, setData1] = useState([
    { img1: images.rockstar, img2: images.user, txt1: '1.50 ETH', },
    { img1: images.rockstar1, img2: images.user, txt1: '1.50 ETH' },
    { img1: images.rockstar2, img2: images.user, txt1: '1.50 ETH' },
    { img1: images.rockstar, img2: images.user, txt1: '1.50 ETH' },
    { img1: images.rockstar2, img2: images.user, txt1: '1.50 ETH' },
    { img1: images.rockstar1, img2: images.user, txt1: '1.50 ETH' },
    { img1: images.rockstar, img2: images.user, txt1: '1.50 ETH' },
    { img1: images.rockstar2, img2: images.user, txt1: '1.50 ETH' }
  ])
  const [colorBG, setbgColor] = useState(false)
  const [show, setShow] = useState(false)

  const header = () => {

    return (


      <View style={{ flex: 1, marginTop: 10, marginTop: 40 }}>

        <View style={{ width: 130, height: 180, backgroundColor: "tranparent", justifyContent: "center", alignItems: "center", right: 50 }}>

          <Typography size={30} style={{ transform: [{ rotate: '-90deg' }] }}>Discover</Typography>

        </View>

      </View>
    );

  }
  return (
    <ScrollContainer withBG bottomView bottomText={'Create NFT'} onBottomPress={()=>{
        navigation.navigate('CreateNft1')
    }}>

      <AuthHeader
       back
       title='NFTS'
       onBackPress={()=>{navigation.goBack()}}
      />
        <View style={{marginTop:10}}>
          <Typography type='bold' size={19} color={colors.redBorderColor} style={{ marginTop: 15 }}> NFT</Typography>
          <FlatList
            data={data1}
            columnWrapperStyle={{ justifyContent: "space-around" }}
            // keyExtractor={item => "#" + item?.id}
            numColumns={2}
            renderItem={({ item, index }) => (

              <Press style={styles.flalist3}>

                <ImageBackground style={{ width: '100%', height: 100, alignSelf: "center", flexDirection: 'column-reverse' }}
                  source={item.img1} imageStyle={{ borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>


                  <View style={{ flexDirection: "row", marginLeft: 10, top: 5 }}>
                    <Image style={{ width: 20, height: 20, borderRadius: 10, }} source={item.img2}></Image>
                    <Typography color='white' size={12} style={{ marginLeft: 5 }}>cooltown</Typography>
                  </View>


                </ImageBackground>

                <View style={{ flexDirection: "row", justifyContent: "space-between", paddingHorizontal: 8, marginTop: 10 }}>
                  <Typography size={11} color='#903126'>Snoop Dog-B.O.D.R</Typography>
                  <Icon
                    size={14}
                    source={images.heart}
                  />
                </View>
                <Typography size={11} style={{ marginLeft: 8 }}>Music</Typography>

                <View style={{ flexDirection: "row", justifyContent: "space-between", paddingHorizontal: 8, }}>
                  <Typography size={11}>Bad Price :</Typography>
                  <Typography size={11} color='#903126'>{item.txt1}</Typography>
                </View>

              </Press>

            )}
          />

        </View>

      
    

    </ScrollContainer>


  )
}
const styles = StyleSheet.create({

  flalist3: {
    width: '48%',
    height: 180,
    backgroundColor: "#222222",
    borderRadius: 10,
    marginTop: 15

  }

})

export default HomeMusicRaver