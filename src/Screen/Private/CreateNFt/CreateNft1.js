import React, {useState} from 'react';
import CommonButton from '../../../Components/UI/CommonButton';
import ButtonRow from '../../../Components/UI/ButtonRow';
import ScrollContainer from '../../../Components/HOC/ScrollContainer';
import Icon from '../../../Components/UI/Icon';
import Typography from '../../../Components/UI/Typography';
import IconButton from '../../../Components/UI/IconButton';
import ProgressBar from '../../../Components/HOC/ProgressBar';
import colors from '../../../Constants/colors';
import {STANDARD_WIDTH} from '../../../Constants/layout';
import {View, Modal} from 'react-native';
import AuthHeader from '../../../Components/HOC/AuthHeader';
import {FlatList} from 'react-native-gesture-handler';
import BlackInput from '../../../Components/UI/TextInput/BlackInput';
import Card from '../../../Components/UI/Card';
import GlobalDropDown from '../../../Components/UI/DroupDown/GlobalDrown';
import Press from '../../../Components/HOC/Press';
import TagModal from '../../../Components/UI/Modal/TagModal';
import fonts from '../../../Constants/fonts';
import images from '../../../Constants/images';
const CreateNft1 = ({navigation}) => {
  const [category, setCategory] = useState([]);
  const [categorySelected, setCategorySelected] = useState([]);
  const [categoryLabel, setCategoryLabel] = useState();
  const [modal, setModal] = useState(false);
  const [tags, setTags] = useState([]);

  const textToShow = (arr, placeholder) => {
    if (arr.length > 0) {
      return tags.join('');
    } else {
      return placeholder;
    }
  };

  return (
    <ScrollContainer
      withBG
      bottomText="Next"
      onBottomPress={() => navigation.navigate('CreateTrack')}
      onBottomPress={() => {
        navigation.navigate('CreateNft2');
      }}
      bottomView >
      <TagModal
        showModal={modal}
        onClose={() => {
          setModal(false);
        }}
        maxLength={25}
        currentLength={textToShow(tags, '').length}
        onSelect={value => {
          var arr = [...tags];
          arr.push(value);
          setTags(arr);
          setModal(false);
        }}
      />
      <AuthHeader
        title="Create New NFT"
        back
        onBackPress={() => {
          navigation.goBack();
        }}
      />
      <ProgressBar
        shouldAnimated={false}
        value={1}
        width={STANDARD_WIDTH - 20}
      />
      <View style={{marginVertical: 20}}>
        <Typography size={17} type="semiBold">
          {'Upload & Create Item info'}
        </Typography>
      </View>
      <Typography size={15} type="semiBold" color={colors.redBorderColor}>
        {'Upload New Image'}
      </Typography>
      <Typography size={12} type="semiBold">
        {'Choose your file to upload'}
      </Typography>

      <Card
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          paddingVertical: 20,
        }}>
        <Icon
          source={images.uploade}
          size={50}
          tintColor={colors.redBorderColor}
        />
        <Typography size={12} style={{marginTop: 10}}>
          PNG,JPG Max 50mb
        </Typography>
      </Card>
      <GlobalDropDown
        categroy={[{label: 'sdad', value: 'sdasdas'}]}
        label={'Category'}
        value={''}
        type={'Select Category'}
        placeholder={'Select Category'}
        callBack={(v, label) => {
          console.log(v, 'asdas');
        }}
      />
      <View>
        <Typography
          type="semiBold"
          color={colors.redBorderColor}
          size={14}
          style={{marginLeft: 10, marginBottom: 3}}>
          Tag ({tags.length})
        </Typography>
        
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Card
            style={{
              paddingHorizontal: 18,
              paddingVertical: 15,
              width: STANDARD_WIDTH - 70,
              alignSelf: 'flex-start',
              height: 50,

              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View style={{width: STANDARD_WIDTH - 160}}>
              <FlatList
                data={tags}
                horizontal={true}
                style={{}}
                contentContainerStyle={{
                  flexDirection: 'row',
                }}
                ListEmptyComponent={() => {
                  return (
                    <Typography size={12} style={{}}>
                      Select Tags
                    </Typography>
                  );
                }}
                renderItem={({item, index}) => {
                  return (
                    <Typography
                      size={13}
                      style={{
                        marginRight: 10,
                        textDecorationLine: 'underline',
                        color:
                          index % 2 == 0 ? colors.white : colors.redBorderColor,
                      }}
                      type="semiBold">
                      {item}
                    </Typography>
                  );
                }}
              />
            </View>
            <Typography size={12}>{textToShow(tags, '').length}/25</Typography>
          </Card>
          <Card
            style={{
              width: 50,
              justifyContent: 'center',
              alignItems: 'center',
              height: 50,
            }}
            disabled={false}
            onPress={() => {
              setModal(true);
            }}>
            <Icon
              size={16}
              imgStyle={{resizeMode: 'contain'}}
              source={images.addIcon}
            />
          </Card>
        </View>
      </View>
      <BlackInput
        placeholder="Snoop Dogg R.F"
        label="Name"
        inputStyle={{fontFamily: fonts.medium}}
        style={{marginTop: 10}}
      />
      <BlackInput
        placeholder="Enter your text here"
        label="Description"
        style={{marginTop: 10}}
        inputStyle={{
          height: 100,
          textAlignVertical: 'top',
          fontSize: 13,
          fontFamily: fonts.medium,
        }}
        multiline={true}
      />
    </ScrollContainer>
  );
};

export default CreateNft1;
