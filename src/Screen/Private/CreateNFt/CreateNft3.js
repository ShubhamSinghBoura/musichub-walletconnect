import React, {useState} from 'react';
import CommonButton from '../../../Components/UI/CommonButton';
import ButtonRow from '../../../Components/UI/ButtonRow';
import ScrollContainer from '../../../Components/HOC/ScrollContainer';
import Icon from '../../../Components/UI/Icon';
import Typography from '../../../Components/UI/Typography';
import Card from '../../../Components/UI/Card';
import IconButton from '../../../Components/UI/IconButton';
import ProgressBar from '../../../Components/HOC/ProgressBar';
import colors from '../../../Constants/colors';
import {STANDARD_WIDTH} from '../../../Constants/layout';
import {View} from 'react-native';
import Press from '../../../Components/HOC/Press';
import AuthHeader from '../../../Components/HOC/AuthHeader';
import {FlatList} from 'react-native-gesture-handler';
import BlackInput from '../../../Components/UI/TextInput/BlackInput';
import images from '../../../Constants/images';

const CreateNft3 = ({navigation}) => {
  const [selected, setSelected] = useState('Fixed Price');
  const [price, setPrice] = useState();
  const [royalties, setRoyalties] = useState();
  return (
    <ScrollContainer
      withBG
      bottomText="Submit"
      onBottomPress={() => {navigation.navigate('NFTS')}}
      bottomView
      >
      <AuthHeader
        title="Create New NFT"
        back
        onBackPress={() => {
          navigation.goBack();
        }}
      />
      <ProgressBar
        shouldAnimated={false}
        value={3}
        width={STANDARD_WIDTH - 20}
      />
      <View style={{marginVertical: 20}}>
        <Typography size={17} type="semiBold">
          {'Final Review'}
        </Typography>
      </View>
      <View
        style={{
          alignSelf: 'center',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Icon source={images.user} size={150} />
        <Typography size={15} type="semiBold" style={{marginVertical: 5}}>
          {'Snoop Dogg R.F'}
        </Typography>
      </View>
      <View style={{marginTop: 0}}>
        <View style={{marginTop: 10}}>
          <Typography size={15} type="semiBold" color={colors.redBorderColor}>
            Category
          </Typography>
          <Typography size={13} type="semiBold" style={{marginVertical: 4}}>
            Music
          </Typography>
        </View>
        <View style={{marginTop: 10}}>
          <Typography size={15} type="semiBold" color={colors.redBorderColor}>
            Description
          </Typography>
          <Typography size={13} type="semiBold" style={{marginVertical: 4}}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s,
          </Typography>
        </View>
        <View style={{marginTop: 10}}>
          <Typography size={15} type="semiBold" color={colors.redBorderColor}>
            Minimum Bid
          </Typography>
          <Typography size={13} type="semiBold" style={{marginVertical: 4}}>
            1.35 ETH / $4,210.30
          </Typography>
        </View>

        <View
          style={{
            alignSelf: 'center',
            width: STANDARD_WIDTH - 50,
            marginTop: 20,
          }}>
          <Typography
            size={13}
            type="medium"
            color={colors.white}
            style={{marginVertical: 4}}
            textAlign="center">
            By continuing you agree to pay relevant fee while transacting
          </Typography>
        </View>
      </View>
    </ScrollContainer>
  );
};

export default CreateNft3;
