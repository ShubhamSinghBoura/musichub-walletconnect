import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import ScrollContainer from '../../Components/HOC/ScrollContainer'
import Typography from '../../Components/UI/Typography'
import colors from '../../Constants/colors'
import AuthHeader from '../../Components/HOC/AuthHeader'

const Soon = ({navigation}) => {
  return (
    <ScrollContainer contentContainerStyle={{flex:1}} withBG>
         <AuthHeader
                back
              
                onBackPress={() => { navigation.goBack() }}
                style
            />
      
      <View style={{flex:1,justifyContent:"center",alignItems:"center"}}>
       <Typography color={colors.redBorderColor} textAlign='center'
       size={22}>Comming Soon</Typography>
       </View>


    </ScrollContainer>
  )
}

export default Soon

const styles = StyleSheet.create({})