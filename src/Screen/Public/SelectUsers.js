import { StyleSheet, Text, View, FlatList, Image, Animated, Dimensions } from 'react-native'
import React, { useEffect, useRef, useState } from 'react'
import ViewContainer from '../../Components/HOC/ViewContainer'
import ScrollContainer from '../../Components/HOC/ScrollContainer'
import Press from '../../Components/HOC/Press'
import Typography from '../../Components/UI/Typography'
import colors from '../../Constants/colors'
import CommonButton from '../../Components/UI/CommonButton'
import images from '../../Constants/images'
import Icon from '../../Components/UI/Icon'
import AsyncStorage from '@react-native-async-storage/async-storage'
const { width, height } = Dimensions.get("window")
import { ARTIST, ARTIST_DASHBOARD, PR, PRODUCER, PRODUCER_DASHBOARD, PR_DASHBOARD, RAVER, RAVER_DASHBOARD } from '../../Constants/userTypes'
import { useAuth, useDashboard, useDrawer } from '../../Context/UserContext'
import SimpleToast from 'react-native-simple-toast'
import { USER_ARTIST, USER_FAN, USER_PR, USER_PRODUCER } from '../../Constants/userDrawerType'

const UserSelection = ({
    onPress = () => { },
    isSelected = false,
    title = '',
    heading
}) => {
    const AnimationValue = useRef(new Animated.Value(0)).current;
    const [isSelectedCheck, setIsSelectedCheck] = useState(false);
    useEffect(() => {
        setIsSelectedCheck(isSelected);
        Animated.timing(AnimationValue, {
            toValue: isSelected ? 1 : 0,
            duration: 200,
            useNativeDriver: true,
        }).start(() => { });
    }, [isSelected, AnimationValue]);

    return (
        <Press scaleSize={0.95} onPress={onPress}>
            <Animated.View
                style={[
                    styles.boxText,
                    {
                        borderColor: isSelectedCheck
                            ? colors.origin
                            : colors.buttonTextSecondary,
                        transform: [
                            {
                                scale: AnimationValue.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [0.95, 1],
                                }),
                            },
                        ],
                    },
                ]}>

                <View>
                    <Typography size={10}>
                        {heading}
                    </Typography  >
                    <Typography color={isSelectedCheck ? colors.origin : colors.white} type="bold" >
                        {title}
                    </Typography>
                </View>

            </Animated.View>
        </Press>
    );
};

const SelectUsers = ({ navigation }) => {
const {   updateUserDrawer } = useDrawer()
    const [SelectedUser, setSelectedUser] = useState('')

    const [selectUser, setSelectUser] = useState([
        {
            id: 0,
            icon: images.producer,
            title: "PRODUCER",
            heading: "Continue as",
            usertype: PRODUCER,
            dashboard:PRODUCER_DASHBOARD,
            drawer : USER_PRODUCER
        },
        {
            id: 1,
            icon: images.pr,
            title: "PR",
            heading: "Continue as",
            usertype: PR,
            dashboard:PR_DASHBOARD,
            drawer : USER_PR
        },
        {
            id: 2,
            icon: images.artist,
            title: "ARTIST",
            heading: "Continue as",
            usertype: ARTIST,
            dashboard:ARTIST_DASHBOARD,
            drawer: USER_ARTIST
        },
        {
            id: 3,
            icon: images.raver,
            title: "RAVER",
            heading: "Continue as",
            usertype: RAVER,
            dashboard:RAVER_DASHBOARD,
            drawer: USER_FAN
        },
    ])
    const {updateinitialDashboard} = useDashboard()
    const {updateIsAuth} = useAuth();
    const onUserSelection = (user,dashboard,drawer) => {
        AsyncStorage.setItem('USER', user)
        setSelectedUser(user)
        updateinitialDashboard(dashboard)
        updateUserDrawer(drawer)

    }
    const onSignup = () => {
        if (SelectedUser) {
            // updateIsAuth(true)
            navigation.navigate("Login")
        } else {
            SimpleToast.show('Please select a user')
        }
    }
    return (
        <ViewContainer>
            <ScrollContainer withBG >

                <View style={{ marginTop: 45, marginBottom: 15 }} >
                    <Typography type="bold" size={18}>Choose user</Typography>
                </View>


                <FlatList
                    data={selectUser}
                    renderItem={({ item, index }) => {
                        return (
                            <View key={index} style={{ flexDirection: index % 2 == 0 ? 'row-reverse' : 'row', justifyContent: 'space-between', alignItems: "flex-end", marginTop: 25, paddingHorizontal: 5 }} >
                                <View style={{}} >
                                    <View style={[styles.box1, {  width: width / 7.5,
                                    height: height * 0.06, alignSelf: "flex-end", }]} />
                                    <View style={[styles.boxText, { width: width / 4.0, height: height * 0.12, borderColor: colors.buttonTextSecondary, }]}  >
                                        <Icon size={50} tintColor={colors.origin} source={item.icon} />
                                    </View>
                                </View>
                                <View style={{    width: width / 2.5, height: height * 0.22,}} >
                                    <UserSelection
                                        title={item.title}
                                        heading={item.heading}
                                        isSelected={SelectedUser == item.usertype}
                                        onPress={() => onUserSelection(item.usertype,item.dashboard,item.drawer)}
                                    />
                                </View>
                                <View>
                                    <View style={[styles.box1, {  width: width / 7.5,
                                     height: height * 0.06,}]} />
                                    <View style={[styles.box1, { marginBottom: 0, }]} />
                                </View>
                            </View>
                        )
                    }}
                />
                <View style={{ height: 70, }} ></View>
            </ScrollContainer>
            <CommonButton
                text={`Sign up`}
                style={{ width: "90%", marginVertical: 25, alignSelf: 'center', borderColor:colors.origin, borderWidth:1, }}
                onPress={()=>onSignup()}
            />
        </ViewContainer>
    )
}

export default SelectUsers

const styles = StyleSheet.create({
    box1: {
        width: width / 6.5,
        height: height * 0.07,
        // width: 50,
        // height: 50,
        borderRadius: 5,
        backgroundColor: colors.buttonTextSecondary,
        marginBottom: 10,
        borderWidth: 0.7,
        borderColor: "#616161"
    },
    boxText: {
           width: width / 2.5,
        height: width / 2.5,
        // width: 125,
        // height: 125,
        borderRadius: 5,
        backgroundColor: colors.secondary,
        justifyContent: "center",
        alignItems: "center",
        borderColor: colors.origin,
        borderWidth: 1,
    },

})



















