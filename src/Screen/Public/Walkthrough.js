import {
  StyleSheet,
  View,
  ImageBackground,
  Image,
  TouchableOpacity,
  Dimensions,
  FlatList
} from 'react-native';
import React, { useState } from 'react';
import ViewContainer from '../../Components/HOC/ViewContainer';
import colors from '../../Constants/colors';
import images from '../../Constants/images';
import Typography from '../../Components/UI/Typography';
import fonts from '../../Constants/fonts';
import { useAuth } from '../../Context/UserContext';

const { width, height } = Dimensions.get("screen")

const Walkthrough = ({ navigation }) => {
  const { updateIsAuth } = useAuth()

  const [content, setContent] = useState([
    {
      id: "1",
      name: <Typography color={colors.white} size={40} type="bold">
        Welcome
        <Typography color={colors.white} size={25} type="bold">
          {' '}
          to the world of{' '}
        </Typography>
        <Typography color={colors.origin} size={25} type="bold">
          Music{' '}
        </Typography>
      </Typography>,
      title: "Plunge to the atmosphere of the music city",
    },
    {
      id: "2",
      name: <Typography color={colors.white} size={40} type="bold">
        Welcome
        <Typography color={colors.white} size={25} type="bold">
          {' '}
          to the world of{' '}
        </Typography>
        <Typography color={colors.origin} size={25} type="bold">
          Music{' '}
        </Typography>
      </Typography>,
      title: "Plunge to the atmosphere of the music city",
    },
    {
      id: "3",
      name: <Typography color={colors.white} size={40} type="bold">
        Welcome
        <Typography color={colors.white} size={25} type="bold">
          {' '}
          to the world of{' '}
        </Typography>
        <Typography color={colors.origin} size={25} type="bold">
          Music{' '}
        </Typography>
      </Typography>,
      title: "Plunge to the atmosphere of the music city",
    },
  ]);
  const [showBTN, setShowBTN] = useState(0);

  return (
    <ViewContainer   >

      <ImageBackground source={images.backgroundbg} style={styles.dotsbg}>

        <View >
          <Image style={{ height: height * 0.5, width: '90%', }} resizeMode={"stretch"} source={images.swiperBg} />
        </View>

        <FlatList
          data={content}
          onScroll={(e) => console.log(setShowBTN((e.nativeEvent.contentOffset.x / width).toFixed()))}
          pagingEnabled
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          renderItem={({ item, index }) => {
            return (
              <View style={styles.slide}>

                {item.name}

                <Typography
                  color={colors.white}
                  size={22}
                  type="regular"
                  style={{ marginVertical: 15, }}>
                  {item.title}
                </Typography>
              </View>
            )
          }}

        />


        <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginBottom: 15 }} >

          <View style={{ flexDirection: 'row', marginLeft: 15 }}>
            {
              content.map((item, i) => {
                return (
                  <View style={{
                    backgroundColor: i == showBTN ? colors.white : colors.origin,
                    width: 10,
                    height: 10,
                    borderRadius: 50,
                    margin: 5
                  }} />
                )
              })
            }
          </View>

          <TouchableOpacity
            onPress={() => {
              updateIsAuth(true)
            }}
          >

            <ImageBackground style={styles.nextBTN} source={images.nextBTN} >
              <Image style={styles.arrowsNEXT} source={images.arrowsNEXT} />
            </ImageBackground>

          </TouchableOpacity>
        </View>

      </ImageBackground>

    </ViewContainer>
  );
};

export default Walkthrough;

const styles = StyleSheet.create({
  dotsbg: {
    flex: 1,
    width: '100%',
  },
  swiperBg: {
    flex: 1,
    paddingHorizontal: 10,
  },

  slide: {
    flex: 1,
    paddingHorizontal: 21,
    width: width
  },
  toptitle: {
    color: colors.lightRed,
    fontFamily: fonts.bold,
    fontSize: 22,
    textAlign: 'center',
  },
  bottomtitle: {
    color: colors.white,
    fontSize: 15,
    fontWeight: "700",
    textAlign: "center",
    marginVertical: 15,
  },

  nextBTN: {
    width: 120,
    height: 120,
    justifyContent: "center",
    alignItems: "center",
  },
  arrowsNEXT: {
    width: 40,
    height: 15,
  }


})
