import {
  Text, View, Button, KeyboardAvoidingView, ImageBackground, Dimensions, Image,
  Animated
} from 'react-native';
import React, { useRef, useState, useEffect } from 'react';
import { useAuth, useDashboard } from '../../Context/UserContext';
import fonts from '../../Constants/fonts';
import FormContainer from '../../Components/HOC/FormContainer';
import Icon from '../../Components/UI/Icon';
import Images from '../../Constants/images';
import Press from '../../Components/HOC/Press';
import ViewContainer from '../../Components/HOC/ViewContainer';
import Typography from '../../Components/UI/Typography';
import CommonInput from '../../Components/UI/CommonInput';
import CommonButton from '../../Components/UI/CommonButton';
import { NavigationContainer } from '@react-navigation/native';
import {  useIsFocused } from "@react-navigation/native";
import ScrollContainer from '../../Components/HOC/ScrollContainer';
import { FULL_WIDTH } from '../../Constants/layout';

const Login = ({navigation}) => {
  const {updateIsAuth} = useAuth();
const {initialDashboard}=useDashboard()
  const [bgColor, setbgColor] = useState(false)
  const [userName, setuserName] = useState()
  const [userName1, setuserName1] = useState()
  const [email, setEmail] = useState()
  const [password, setPassword] = useState()
  const [password1, setPassword1] = useState()
  const [confirm, setConfirm] = useState()
  const [showLogin, setshowLogin] = useState(true)
  const isFocus = useIsFocused()
  useEffect(()=>{
    setbgColor(false)
   setshowLogin(true)
  },[isFocus])
  return (
    <ImageBackground source={Images.loginBackground} style={{
      flex: 1, width: FULL_WIDTH,
    }}>
      <FormContainer backgroundColor='transparent' bottomView={false} style={{ }} >
        {/* <Image source={Images.dots} resizeMode="cover"
          style={{
            width: Dimensions.get('screen').width / 1.7,
            height: Dimensions.get('screen').width / 1.1, 
            position: "absolute", 
            right: 0, 
            top: 0,
          
          }}
        /> */}

        <Icon
          source={Images.logo1}
          size={170}
          style={{
            alignSelf: 'center',
            marginTop: 50
          }} />

        <View style={{
          width: '90%',
          backgroundColor: '#222222',
          alignSelf: 'center',
          borderRadius: 10,
          marginTop: 30
        }}>

          <View style={{
            flexDirection: 'row',
            paddingVertical: 15,
            flex: 1,
            justifyContent:"space-between",
            
          }}>

            <Press 
              onPress={() => {
                setbgColor(false)
               setshowLogin(true)
              }}
              scaleSize={1.1}
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
              }}>
              <Typography 
              type='semiBold'
              size={15} 
               style={{ alignSelf: 'center', color: bgColor == true ? 'white' : '#903126' 
               }}>LOGIN</Typography>
            </Press>

              <View style={{width:2,height:30,backgroundColor:"white"}}></View>

            <Press
              scaleSize={1.1}
              onPress={() => {
                setbgColor(true)
               setshowLogin(false)
              }}
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
               
              }}>
              <Typography 
              type='medium'
              size={15} 
              style={{ alignSelf: 'center', color: bgColor == true ? '#903126' : 'white' }}>SIGN UP</Typography>
            </Press>

          </View>


        </View>

        <View style={{ marginTop: 40 }}>

          <Typography type='semiBold' color='white' size={25} style={{ textAlign: 'center' }}>Welcome User</Typography>
          <Typography type='regular' color='white' style={{ textAlign: 'center' }}>Please create your account </Typography>

        </View>
        <View style={{ marginTop: 30 }}>

          {showLogin ?
            <View>
              <CommonInput
                label='Username'
                value={userName1}
                Images={Images.name}
                onChangeText={(e) => {
                  setuserName1(e)
                }} />
              <CommonInput
                label='Password'
                value={password1}
              
                Images={Images.password1}
                onChangeText={(e) => {
                  setPassword1(e)
                }} />

              <CommonButton backgroundColor='#d83e26'
                onPress={()=>{
                 navigation.navigate("Walkthrough")
                // console.log('---->',initialDashboard);
                }}
                text='Login'
                style={{ width: '90%', height: 50 ,marginTop:40}} />
            </View>
            :

            <View style={{ marginTop: 30 }}>
              <CommonInput
                label='Username'
                value={userName}
               
                Images={Images.name}
                onChangeText={(e) => {
                  setuserName(e)
                }} />
              <CommonInput
                label='Email address'
                value={email}
                
                Images={Images.email}
                onChangeText={(e) => {
                  setEmail(e)
                }} />
              <CommonInput label='Password'
                
                value={password}
                Images={Images.password1}
                secureTextEntry={true}
                onChangeText={(e) => {
                  setPassword(e)
                }} />
              <CommonInput label=' Confirm Password'
                value={confirm}
                
                Images={Images.password1}
                secureTextEntry={true}
                onChangeText={(e) => {
                  setConfirm(e)
                }} />

              <CommonButton backgroundColor='#d83e26'
                text='Sign Up'
                onPress={()=>{
                  navigation.navigate("SelectUsers")
                }}

                style={{ width: '90%', height: 50 ,marginTop:40}} />
            </View>



          }
        </View>


        <View style={{ height: 50 }}></View>

      </FormContainer>
    </ImageBackground>



  );
};

export default Login;
