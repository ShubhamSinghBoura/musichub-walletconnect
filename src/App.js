import React from 'react';
import { UserProvider } from './Context/UserContext';
import 'react-native-gesture-handler';
import MainNavigation from './Navigation/MainNavigation';
import SplashScreen from 'react-native-splash-screen';
import { View, StatusBar, Platform } from 'react-native';
import colors from './Constants/colors';
import { FULL_WIDTH } from './Constants/layout';
const App = () => {
  React.useEffect(() => {
    setTimeout(() => {
      SplashScreen.hide();
    }, 1000);
  }, []);
  return (
   
      <UserProvider>
        <MainNavigation />
      </UserProvider>
   );
};

export default App;
