export default {
  primary: '#242F9B',
  secondary: '#1a1d24',
  buttonTextPrimary: '#222222',
  buttonTextSecondary: '#2d2d2d',
  white: '#ffffff',
  black: '#000000',
  inputBox: '#222',
  redBorderColor: '#cb393a',
  greyColor: 'grey',
  bgColor: '#0a0a0a',
  origin: '#d93f27',
  doctBgColor: '#262a35',
};
