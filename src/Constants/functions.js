export const renderIf = (condition, Element = () => {}) => {
  if (condition !== null && condition !== undefined && condition !== '') {
    return Element;
  }
};
