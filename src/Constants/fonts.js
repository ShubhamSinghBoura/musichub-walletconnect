export default {
  regular: 'poppins-regular',
  medium: 'poppins-medium',
  semiBold: 'poppins-semibold',
  bold: 'poppins-bold',
  light: 'poppins-light',
};
