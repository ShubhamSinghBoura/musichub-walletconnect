export const USER_PRODUCER = [
    {
        name: 'Home',
        routeName: 'Producer',
    },
    {
        name: 'My Profile',
        routeName: 'myAccount',
    },
    {
        name: 'My Playlist',
        routeName: 'MyPlayList',
    },
    {
        name: 'Artists',
        routeName: 'Artist',
    },
    {
        name: 'NFTS',
        routeName: 'NFTS',
    },
    
    {
        name: 'Create Event',
        routeName: 'Soon',
    },

    {
        name: 'Notifications',
        routeName: 'Notification',
    },
    {
        name: 'Transactions',
        routeName: 'Transaction',
    },
    // {
    // name: 'Beverages',
    // routeName: 'Beverages',
    // },
    // {
    // name: 'Beverages Orders',
    // routeName: 'BeveragesOrders',
    // },
    // {
    // name: 'Staff Management',
    // routeName: 'StaffManagement',
    // },
    // {
    // name: 'Transactions',
    // routeName: 'Transactions',
    // },
    // {
    // name: 'My Wallet',
    // routeName: 'WalletModule',
    // },
    // {
    // name: 'Treading History',
    // routeName: 'TreadingHistory',
    // },
];

export const USER_ARTIST = [
    {
        name: 'Home',
        routeName: 'HomeNFT',
    },
    {
        name: 'My Profile',
        routeName: 'myAccount',
    },
    {
        name: 'Collections ',
        routeName: 'MyPlayList',
    },

    // {
    // name: 'My Events',
    // routeName: 'MyEvents',
    // },
    
     {
    name: 'Create Nfts',
    routeName: 'CreateNft1',
    },
    {
        name: 'Transactions',
        routeName: 'Transaction',
    },
    {
        name: 'Notifications',
        routeName: 'Notification',
    },
    
    // {
    // name: 'My Wallet',
    // routeName: 'WalletModule',
    // },
    // {
    // name: 'Treading History',
    // routeName: 'TreadingHistory',
    // },
];

export const USER_PR = [
    {
        name: 'Home',
        routeName: 'HomeMusicPR',
    },
    {
        name: 'My Profile',
        routeName: 'myAccount',
    },
    {
        name: 'Notifications',
        routeName: 'Notification',
    },
    {
        name: 'My Albums',
        routeName: 'MyPlayList',
    },
    // {
    // name: 'My Events',
    // subRoute: [
    // {
    // name:'Current events',
    // routeName:'EventsListing'
    // },
    // {
    // name:'Past events',
    // routeName:'EventsListing'
    // },
    // {
    // name:'Future events',
    // routeName:'EventsListing'
    // },
    // // {
    // // name:'Popular events',
    // // routeName:'EventsListing'
    // // },
    // ]
    // },
    // {
    // name: 'Nfts',
    // routeName: 'NFTS',
    // },
    // {
    // name: 'My Wallet',
    // routeName: 'WalletModule',
    // },
    // {
    // name: 'Payment Mangement',
    // routeName: 'PaymentMangement',
    // },
    {
        name: 'Transactions',
        routeName: 'Transaction',
    },
    // {
    // name: 'Treading History',
    // routeName: 'TreadingHistory',
    // },
];

export const USER_FAN = [
    {
        name: 'Home',
        routeName: 'HomeMusicRaver'
    },
    {
        name: 'My Profile',
        routeName: 'myAccount'
    },
    {
        name: 'My Playlist',
        routeName: 'MyPlayList',
    },
    {
        name: 'Favourites',
        routeName: 'Fevorites'
    },
    {
        name: 'Explore',
        routeName: 'Explore'
    },
    {
        name: 'Notifications',
        routeName: 'Notification',
    },
    {
        name: 'Transactions',
        routeName: 'Transaction',
    },
    // {
    // name: 'Events',
    // subRoute: [
    // {
    // name:'Popular events',
    // routeName:'EventsListing'
    // },
    // {
    // name:'Upcoming events',
    // routeName:'EventsListing'
    // },
    // ]
    // },
    // {
    // name: 'Nfts',
    // routeName: 'NFTS'
    // },
    // {
    // name: 'Collections',
    // routeName: 'Collections'
    // },
    // {
    // name:'Book Ticket',
    // routeName:'TicketBooking'
    // },
    // {
    // name: 'Purchase EMT',
    // routeName: 'WalletModule'
    // },
    // {
    // name:'Transactions',
    // routeName:'Transactions'
    // },
    // {
    // name: 'My Wallet',
    // routeName: 'WalletModule'
    // },
    // {
    // name: 'Treading History',
    // routeName: 'TreadingHistory'
    // }
]
