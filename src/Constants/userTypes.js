// User Types

export const PRODUCER = 'PRODUCER'
export const PR = 'PR'
export const ARTIST = 'ARTIST'
export const RAVER = 'RAVER'

// Dashboards

export const PRODUCER_DASHBOARD = 'Producer'
export const PR_DASHBOARD = 'HomeMusicPR'
export const ARTIST_DASHBOARD = 'HomeNFT'
export const RAVER_DASHBOARD = 'HomeMusicRaver'