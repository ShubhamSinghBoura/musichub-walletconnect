import {Dimensions} from 'react-native';
export const SPACING = 40;
export const STANDARD_WIDTH = Dimensions.get('screen').width - SPACING;
export const STANDARD_HEIGHT = Dimensions.get('screen').height - SPACING;

export const FULL_WIDTH = Dimensions.get('screen').width;
export const RADIUS = 6;
export const FULL_HEIGHT = Dimensions.get('screen').height;
