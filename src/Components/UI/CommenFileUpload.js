import {StyleSheet, View, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import colors from '../../Constants/colors';
import Typography from './Typography';
import images from '../../Constants/images';

const CommenFileUpload = ({
  uploadeIcon = images.uploade,
  uploadeIconText = 'Un Tagged WAV or MP3',
  uploadeIconSize = '.WAV (or.MP3)',
  uploadeBg,
}) => {
  return (
    <View>
      <TouchableOpacity style={[styles.uploadeBg, {...uploadeBg}]}>
        <Image style={styles.uploadeIcon} source={uploadeIcon} />

        {
          (uploadeIconText,
          uploadeIconSize && (
            <View>
              <Typography
                type="medium"
                color={colors.white}
                size={10}
                textAlign="center"
                style={{marginTop: 10}}>
                {' '}
                {uploadeIconText}{' '}
              </Typography>
              <Typography
                type="medium"
                color={colors.white}
                size={10}
                textAlign="center">
                {' '}
                {uploadeIconSize}{' '}
              </Typography>
            </View>
          ))
        }
      </TouchableOpacity>
    </View>
  );
};

export default CommenFileUpload;

const styles = StyleSheet.create({
  uploadeBg: {
    backgroundColor: colors.buttonTextPrimary,
    alignItems: 'center',
    justifyContent: 'center',
    height: 125,
    borderRadius: 10,
    marginVertical: 7,
  },
  uploadeIcon: {
    width: 50,
    height: 40,
    tintColor: colors.origin,
  },
});
