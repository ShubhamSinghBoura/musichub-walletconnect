import React, {useState} from 'react';
import {View, Text, Modal} from 'react-native';
import colors from '../../../Constants/colors';
import {STANDARD_WIDTH} from '../../../Constants/layout';
import Typography from '../Typography';
import BlackInput from '../TextInput/BlackInput';
import CommonButton from '../CommonButton';
import Card from '../Card';
import Icon from '../Icon';

const TagModal = ({
  showModal,
  currentLength,
  maxLength,
  onClose = () => {},
  onSelect = () => {},
}) => {
  const [text, setText] = useState('');

  return (
    <Modal visible={showModal} transparent={true}>
      <View
        style={{
          flex: 1,
          backgroundColor: 'rgba(0,0,0,0.8)',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            backgroundColor: colors.inputBox,
            width: STANDARD_WIDTH,
            borderRadius: 10,
            padding: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: STANDARD_WIDTH - 30,
              alignItems: 'center',
            }}>
            <Typography size={19} type="medium">
              Select Tag
            </Typography>
            <Icon
              source={require('../../../../Assets/images/arrow.png')}
              size={20}
              touchable
              onPress={() => {
                onClose();
              }}
            />
          </View>

          <Card
            style={{
              paddingHorizontal: 5,
              paddingVertical: 0,
              backgroundColor: colors.white,
              width: STANDARD_WIDTH - 40,
              alignSelf: 'flex-start',
              height: 50,
              paddingRight: 15,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            {console.log(maxLength - (currentLength + text.length))}
            <BlackInput
              inputStyle={{
                fontSize: 14,
                color: colors.black,
                backgroundColor: colors.white,
                width: STANDARD_WIDTH - 100,
                zIndex: 99,
                height: 50,
                marginTop: 0,
              }}
              maxLength={maxLength - currentLength}
              value={text}
              placeholder="Enter tag here"
              placeholderColor={colors.black}
              onChangeText={e => {
                setText(e);
              }}
            />
            <Typography size={12} color={colors.black}>
              {text.length}/{maxLength - currentLength}
            </Typography>
          </Card>

          <CommonButton
            style={{
              backgroundColor: colors.white,
              width: STANDARD_WIDTH - 50,
              height: 40,
            }}
            textColor={colors.black}
            text="Submit"
            onPress={() => {
              onSelect(text);
              setText('');
            }}
          />
        </View>
      </View>
    </Modal>
  );
};

export default TagModal;
