import {StyleSheet, View} from 'react-native';
import React from 'react';
import Press from '../HOC/Press';
import Typography from './Typography';
import colors from '../../Constants/colors';
import {SPACING} from '../../Constants/layout';
import Icon from './Icon';
import globalStyle from '../../Constants/globalStyle';
import {renderIf} from '../../Constants/functions';

const IconButton = ({
  text = 'Press Me',
  onPress = () => {},
  loading = false,
  disable = false,
  backgroundColor = colors.secondary,
  style = {},
  source = null,
  textColor = colors.buttonTextPrimary,
}) => {
  const buttonDisable = disable || loading;
  return (
    <Press
      onPress={onPress}
      disable={buttonDisable}
      style={[
        globalStyle.button,
        styles.button,
        styles.fullButton,
        {backgroundColor},
        style,
      ]}>
      {renderIf(
        source,
        <View
        style={{
          // marginHorizontal:5
        }}
        >

        <Icon
        tintColor={textColor}
          loading={loading}
          size={23}
          disable={true}
          source={source}
          loaderColor={textColor}
          />
          </View>
          ,
      )}
      {renderIf(
        text,
        <Typography size={16} color={textColor} style={styles.typography}>
          {text}
        </Typography>,
      )}
    </Press>
  );
};

export default IconButton;

const styles = StyleSheet.create({
  button: {
    paddingHorizontal: SPACING,
  },
  fullButton: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  typography: {
    marginHorizontal: 5,
  },
});
