import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import Press from '../../HOC/Press';
import Typography from '../Typography';
import {SPACING} from '../../../Constants/layout';
import globalStyle from '../../../Constants/globalStyle';

const BottomSheetItem = ({
  title = '',
  onPress = () => {},
  noBorder = false,
  cancel = false,
}) => {
  return (
    <Press
      style={[
        styles.item,
        noBorder ? globalStyle?.noBorder : {},
        cancel ? styles.cancelItem : {},
      ]}
      scaleSize={0.995}
      onPress={onPress}>
      <Typography color={!cancel ? '#000' : '#f00'}>{title}</Typography>
    </Press>
  );
};

export default BottomSheetItem;

const styles = StyleSheet.create({
  item: {
    paddingVertical: SPACING,
    borderBottomWidth: 0.5,
    borderBottomColor: '#00000020',
  },
  cancelItem: {},
});
