import {StyleSheet, ActivityIndicator} from 'react-native';
import React from 'react';
import ViewContainer, {containerStyle} from '../HOC/ViewContainer';
import colors from '../../Constants/colors';

const Loader = ({loading = false}) => {
  return (
    <ViewContainer
      style={[containerStyle.container(colors.primary), styles.loader]}>
      {loading && <ActivityIndicator size={'large'} color={colors.white} />}
    </ViewContainer>
  );
};

export default Loader;

const styles = StyleSheet.create({
  loader: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
