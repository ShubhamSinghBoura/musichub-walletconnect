import {StyleSheet, View} from 'react-native';
import React from 'react';
import {SPACING, STANDARD_WIDTH} from '../../Constants/layout';
import colors from '../../Constants/colors';
import Press from '../HOC/Press';
const Card = ({
  children,
  style = {},
  radius,
  disabled = true,
  onPress = () => {},
}) => {
  return (
    <Press
      disable={disabled}
      style={[styles.card, style]}
      onPress={() => onPress()}>
      {children}
    </Press>
  );
};

export default Card;

const styles = StyleSheet.create({
  card: {
    width: STANDARD_WIDTH,
    alignSelf: 'center',
    marginVertical: 10,
    backgroundColor: colors.inputBox,
    padding: 10,
    borderRadius: 10,
  },
});
