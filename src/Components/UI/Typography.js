import {StyleSheet, Text} from 'react-native';
import React from 'react';
import fonts from '../../Constants/fonts';
import colors from '../../Constants/colors';

const Typography = ({
  size = 16,
  children,
  type = 'regular',
  color = colors.white,
  textAlign = undefined,
  style = {},
  numberOfLines,
 
  ...props
}) => {
  return (
    <Text
      style={[styles.font(type), {fontSize: size, color, textAlign}, style]}
      numberOfLines={numberOfLines}
    
      {...props}>
      {children}
    </Text>
  );
};

export default Typography;

const styles = StyleSheet.create({
  font: type => ({
    fontFamily: fonts[type] || undefined,
    textAlignVertical: 'center',
    includeFontPadding: false,
  }),
});
