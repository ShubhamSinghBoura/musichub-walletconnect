import {StyleSheet, ActivityIndicator} from 'react-native';
import React from 'react';
import Press from '../HOC/Press';
import Typography from './Typography';
import colors from '../../Constants/colors';
import {SPACING, STANDARD_WIDTH} from '../../Constants/layout';
import globalStyle from '../../Constants/globalStyle';
import {renderIf} from '../../Constants/functions';

const CommonButton = ({
  text = 'Press Me',
  onPress = () => {},
  loading = false,
  disable = false,
  backgroundColor = colors.inputBox,
  style = {},
  halfButton = false,
  fullButton = false,
  textColor = colors.white,
}) => {
  const buttonDisable = disable || loading;
  const buttonStyle = [
    globalStyle.button,
    styles.button,
    halfButton ? styles.halfButton : {},
    fullButton ? styles.halfButton : {},
    {backgroundColor},
    style,
  ];
  return (
    <Press
      scaleSize={1.05}
      onPress={onPress}
      disable={buttonDisable}
      style={[buttonStyle]}>
      {/* {renderIf(
        text,
        loading ? (
          <ActivityIndicator color={textColor} size="small" />
        ) : (
          <Typography size={16} type='medium' color={textColor} style={{marginTop:0}}>{text}</Typography>
        ),
      )}
        // loading ? (
        //   <ActivityIndicator color={textColor} size="small" />
        // ) : ( */}
      <Typography type="semiBold" color={textColor}>
        {text}
      </Typography>
      {/* // ),
      )} */}
    </Press>
  );
};

export default CommonButton;

const styles = StyleSheet.create({
  button: {
    // paddingVertical: SPACING - 3,
    // paddingHorizontal: 43 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
  },
  fullButton: {
    width: '100%',
  },
  halfButton: {
    width: '50%',
  },
});
