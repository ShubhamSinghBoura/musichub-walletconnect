import { View, Text, TextInput ,Dimensions} from 'react-native'
import React, { useState } from 'react'
import Icon from './Icon'
import Images from '../../Constants/images'
import Typography from './Typography'
import { color } from 'react-native-reanimated'



const CommonInput = ({


  placeHolder,
  value,
  keyboardType,
  placeholderTextColor = "#000",
  inputStyle = {},
  label,
  Images,
  secureTextEntry,
  type,

  ...props


}) => {

  const [isFocuse, setisFocuse] = useState(false)

  return (

    <View
    style={{
      width:Dimensions.get('screen').width-20,alignSelf:"center",
      marginBottom:10
    }}
    >
          <View
        style={{
          position:"relative",
          marginLeft: 40,
          top: isFocuse||!!value ? 5 : 35,
          zIndex:-1,
          elevation:0
        }}
        >

        <Typography 
        
        size={14}
        type='regular'
        style={{
          
        marginLeft:10 ,color:!isFocuse ?'white':'#903126'}}>{label}
        </Typography>
        </View>
      <View
      style={{
        width:Dimensions.get('screen').width-20,alignSelf:"center",
        flexDirection:"row",justifyContent:"flex-start",alignItems:"center"
      }}
      >
      <Icon size={22}  source={Images} style={{left:19}} />
        <TextInput
        placeholder={placeHolder}
        value={value}
        keyboardType={keyboardType}
        onFocus={() => { setisFocuse(true) }}
        onBlur={() => { setisFocuse(false) }}
        
        secureTextEntry={secureTextEntry}
        

        onChangeText={(e) => {
          onChangeText(e)
          
        }}
        
        placeholderTextColor={placeholderTextColor}
        
        style={{
          
          // width: '80%',
          width:Dimensions.get('screen').width-20-40,
          // height: 50,
          padding:10,
          backgroundColor: 'transparent',
          // backgroundColor:"#f0f",
          borderBottomWidth: 1,
          borderBottomColor: 'white',
          alignSelf: 'center',
          color: 'white',
          paddingLeft: 28,
          zIndex:1,
          position:"relative",
          elevation:1,
          fontWeight:"700",
        
          
          
          
          ...inputStyle
        }}
        
        {...props}
        />
      </View>
    </View>




  )
}

export default CommonInput