import React from 'react';
import {View, Text, TextInput} from 'react-native';
import colors from '../../../Constants/colors';
import {STANDARD_WIDTH} from '../../../Constants/layout';
import fonts from '../../../Constants/fonts';
import Typography from '../Typography';

const BlackInput = ({
  label,
  placeholder,
  value,
  placeholderColor = 'white',
  style = {},
  inputStyle = {},
  onChangeText = () => {},
  maxLength,
  multiline = false,
}) => {
  return (
    <View style={[style]}>
      {label && (
        <Typography
          type="semiBold"
          color={colors.redBorderColor}
          size={14}
          style={{marginLeft: 10, marginBottom: 3}}>
          {label}
        </Typography>
      )}
      <TextInput
        multiline={multiline}
        value={value}
        maxLength={maxLength}
        placeholder={placeholder}
        placeholderTextColor={placeholderColor}
        style={{
          backgroundColor: colors.inputBox,
          width: STANDARD_WIDTH,
          borderRadius: 15,
          color: colors.white,
          fontFamily: fonts.regular,
          paddingHorizontal: 15,
          paddingVertical: 15,
          fontSize: 14,

          ...inputStyle,
        }}
        onChangeText={e => onChangeText(e)}
      />
    </View>
  );
};

export default BlackInput;
