import * as React from 'react';
import {useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Image,
  Dimensions,
} from 'react-native';

import {Picker} from '@react-native-picker/picker';
import fontFamily from '../../../Constants/fonts';
import SimpleToast from 'react-native-simple-toast';
import Typography from '../Typography';
import colors from '../../../Constants/colors';

const GlobalDropDown = ({
  newRef,
  label,
  mandatory = true,
  previous,
  categroy = [],
  style,
  callBack = () => {},
  type,
  textStyle,
  value,
  previousValue = 'not Required',
  edit = true,
  selectedValue = null,
  color = 'white',
  placeholder,
}) => {
  const pickerRef = React.useRef(null);
  const [show, setShow] = React.useState(false);
  const [data, setData] = React.useState([]);

  const openDrop = () => {
    if (previousValue) {
      if (edit) {
        pickerRef.current.focus();
      }
      setShow(!show);
    } else {
      SimpleToast.show(`Select ${previous} First`);
    }
  };
  React.useEffect(() => {
    setData(categroy);
    console.log(label, 'label MOdel ', type);
  }, [categroy]);

  return (
    <View
      style={{
        marginVertical: 10,
      }}>
      {label && (
        <Typography
          type="semiBold"
          color={colors.redBorderColor}
          size={14}
          style={{marginLeft: 5}}>
          {label}
        </Typography>
      )}
      <TouchableOpacity
        accessible
        style={{...styles.container, ...style}}
        onPress={() => openDrop()}
        disabled={!edit}>
        {/* <TextInput ref={newRef} /> */}
        <Text
          numberOfLines={1}
          style={{...styles.textInputButtonTExt, ...textStyle, flex: 1}}>
          {`${placeholder}` || `${type}${mandatory ? '*' : ''}`}
        </Text>
        {/* <Image source={Icons.downArrow} style={{ height: 16, width: 16 }} resizeMode={'contain'} /> */}
        {edit && (
          <Picker
            ref={pickerRef}
            // height={10}

            style={{
              fontSize: 14,
              color: colors.white,
              fontFamily: fontFamily.light,
              textAlignVertical: 'center',
              // backgroundColor: 'red',
              // marginTop: Dimensions.get('window').height * 0.52
              // display: 'none',
              // right: 50
            }}
            itemTextStyle={{fontSize: 0}}
            selectedValue={selectedValue}
            dropdownIconColor={color}
            // mode='dropdown'

            //selectedValue={selectedLanguage}

            onValueChange={
              (itemValue, itemIndex) => {
                // console.log('HII', itemValue, type)
                if (itemValue != undefined) {
                  console.log({itemValue}, 'Value');
                  // let value = { id: itemValue.id, name: type === 'Country' ? itemValue.country_name : type == 'State' ? itemValue.name : type == 'Select Serivice Provider Name' ? itemValue.organization_name : type == 'Select Salutation' ? itemValue.code : itemValue.title }
                  // if (type == 'Dial Code') {
                  // 	value = itemValue
                  // }

                  // setSelected(itemValue)
                  // console.log(value,'Val');
                  callBack(itemValue, categroy[itemIndex - 1].label);
                }
              }
              //setSelectedLanguage(itemValue)
            }>
            <Picker.Item
              label={type}
              value={null}
              style={{
                fontFamily: fontFamily.bold,
              }}
              enabled={false}
            />

            {
              // data.map((ele) =>
              // 	<Picker.Item label={type === 'Country' ? ele.country_name : type == 'State' ? ele.name : type == 'Select Serivice Provider Name' ? ele.organization_name : type == 'Select Salutation' ? ele.code : ele.title} value={ele} />
              // )
              data.map(ele => (
                <Picker.Item label={ele.label} value={ele.value} />
              ))
            }
          </Picker>
        )}
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  textInputButtonTExt: {
    fontSize: 14,
    color: colors.white,
    fontFamily: fontFamily.medium,
    textAlignVertical: 'center',
    width: '70%',
  },
  textInputButton: {},
  container: {
    height: 50,
    borderRadius: 7,
    paddingLeft: 15,
    backgroundColor: colors.inputBox,
    marginTop: 3,
    //justifyContent: "center",
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 0,
  },
});

export default GlobalDropDown;
