import * as React from 'react';
import {useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  Image,
  Modal,
  Dimensions,
  Button,
} from 'react-native';
import colors from '../../../Constants/colors';

import {Picker} from '@react-native-picker/picker';
import fontFamily from '../../../Constants/fonts';
import SimpleToast from 'react-native-simple-toast';
// import {STANDARD_HEIGHT, STANDARD_WIDTH} from '../../Constant/Global';

import {TouchableOpacity} from 'react-native-gesture-handler';
import {STANDARD_WIDTH, STANDARD_HEIGHT} from '../../../Constants/layout';
import Typography from '../Typography';

function GlobalDropDown({
  label = null,
  mandatory = true,
  categroy = [],
  style,
  callBack = () => {},
  type,
  textStyle,
  value,
  edit = true,
  selectedValue = null,
  placeholder,
}) {
  const [selected, setSelected] = React.useState({id: '0', name: ''});
  const pickerRef = React.useRef(null);
  // const [show, setShow] = React.useState(false)
  const [data, setData] = React.useState([]);

  const openDrop = () => {
    // if (edit) {
    // pickerRef.current.focus();
    // }
    console.log('Hiiii===>');

    // setShow(!show)
    setShowModal(true);
  };
  React.useEffect(() => {
    // console.log(categroy);

    setData(categroy);
    setSelected({});
  }, [categroy]);
  const [showModal, setShowModal] = useState(false);
  return (
    <View
      style={{
        marginVertical: 10,
      }}>
      {label && (
        <Typography
          type="semiBold"
          color={colors.redBorderColor}
          size={14}
          style={{marginLeft: 5}}>
          {label}
        </Typography>
      )}
      <TouchableOpacity
        style={{...styles.container, ...style}}
        onPress={() => {
          !edit ? null : openDrop();
        }}>
        <Text
          numberOfLines={1}
          style={{...styles.textInputButtonTExt, ...textStyle, flex: 1}}>
          {`${placeholder}` || `${type}${mandatory ? '*' : ''}`}
        </Text>
        {/* <Image source={Icons.downArrow} style={{ height: 16, width: 16 }} resizeMode={'contain'} /> */}
        {showModal && (
          <Modal
            visible={showModal}
            onRequestClose={() => {
              setShowModal(false);
            }}
            animationType="slide"
            transparent={true}>
            <View
              style={{
                flex: 1,
              }}>
              <TouchableOpacity
                onPress={() => {
                  setShowModal(false);
                }}
                style={{
                  ...StyleSheet.absoluteFillObject,
                }}
              />
              <View
                style={{
                  backgroundColor: '#fff',
                  width: STANDARD_WIDTH,
                  position: 'absolute',
                  bottom: 0,
                }}>
                <View
                  style={{
                    width: STANDARD_WIDTH,
                    alignSelf: 'center',
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                  }}>
                  <Button
                    title="Close"
                    onPress={() => {
                      setShowModal(false);
                    }}
                  />
                </View>
                <Picker
                  // ref={pickerRef}
                  style={{
                    fontSize: 14,
                    color: colors.white,
                    fontFamily: fontFamily.light,
                    textAlignVertical: 'center',
                    // marginTop: STANDARD_HEIGHT*0.2
                    // display: 'none',
                  }}
                  itemTextStyle={{fontSize: 0}}
                  selectedValue={selectedValue || selected}
                  dropdownIconColor={colors.background}
                  //selectedValue={selectedLanguage}

                  onValueChange={
                    (itemValue, itemIndex) => {
                      // console.log('HII', itemValue, type)
                      if (itemValue != undefined) {
                        console.log({itemValue});
                        // let value = { id: itemValue.id, name: type === 'Country' ? itemValue.country_name : type == 'State' ? itemValue.name : type == 'Select Serivice Provider Name' ? itemValue.organization_name : type == 'Select Salutation' ? itemValue.code : itemValue.title }
                        // if (type == 'Dial Code') {
                        // 	value = itemValue
                        // }

                        setSelected(itemValue);
                        // console.log(value,'Val');
                        callBack(itemValue, categroy[itemIndex - 1].label);
                      }
                    }
                    //setSelectedLanguage(itemValue)
                  }>
                  <Picker.Item
                    label={type}
                    value={null}
                    style={{
                      fontFamily: fontFamily.bold,
                    }}
                    enabled={false}
                  />

                  {
                    // data.map((ele) =>
                    // 	<Picker.Item label={type === 'Country' ? ele.country_name : type == 'State' ? ele.name : type == 'Select Serivice Provider Name' ? ele.organization_name : type == 'Select Salutation' ? ele.code : ele.title} value={ele} />
                    // )
                    data.map(ele => (
                      <Picker.Item label={ele.label} value={ele.value} />
                    ))
                  }
                </Picker>
              </View>
            </View>
          </Modal>
        )}
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  textInputButtonTExt: {
    fontSize: 14,
    color: colors.white,
    fontFamily: fontFamily.semiBold,
    textAlignVertical: 'center',
  },
  textInputButton: {},
  container: {
    height: 50,
    borderRadius: 7,
    paddingLeft: 15,
    backgroundColor: colors.inputBox,
    marginTop: 3,
    //justifyContent: "center",
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 0,
  },
  addTicketBtn: {
    width: '40%',
    alignSelf: 'center',
    padding: 13,
    alignItems: 'center',
    backgroundColor: colors.black,
    borderRadius: 7,
  },
  addTicketText: {
    fontSize: 16,
    fontFamily: 'Inter-Medium',
    color: 'white',
  },
  TextInputSection: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 20,
    flex: 1,
  },
});

export default GlobalDropDown;
