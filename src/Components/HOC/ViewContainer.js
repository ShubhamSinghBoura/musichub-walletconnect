import { StyleSheet, SafeAreaView, View } from 'react-native';
import React from 'react';
import Container from './Container';
import colors from '../../Constants/colors';

const ViewContainer = ({
  children,
  backgroundColor = colors.black,
  style = {},
}) => {
  return (
    <View style={[containerStyle.container(backgroundColor), style]}>
      {children}
    </View>
  );
};

export default ViewContainer;

export const containerStyle = StyleSheet.create({
  container: backgroundColor => {
    return { flex: 1, zIndex: 9, backgroundColor };
  },
});
