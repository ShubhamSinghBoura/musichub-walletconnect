import React from 'react';
import { View, Text } from 'react-native';
import Press from './Press';
import Icon from '../UI/Icon';
import colors from '../../Constants/colors';
import Typography from '../UI/Typography';
import images from '../../Constants/images';
import { useNavigation } from '@react-navigation/core';

const AuthHeader = ({
  title,
  back,
  backFilter,
  onBackPress = () => {},
  filterIcon,
  shopIcon,
  arrows,
  close,
  style = {},
  ...props
}) => {
  const navigation = useNavigation();
  return (
    <View style={{ flexDirection: 'row', justifyContent: 'space-between'}}>
      <View style={{ flexDirection: 'row' }}>
        {back && (
          <Press
            style={{
              marginRight: 15,
              padding: 10,
              borderColor: colors.white,
              borderWidth: 1,
              alignSelf: 'flex-start',
              borderRadius: 10,
            }}
            onPress={() => onBackPress()}>
              
            <Icon
              source={backFilter ? require('../../../Assets/images/filter.png') : images.arrow }
              size={13}
              imgStyle={{ resizeMode: 'contain' }}
            />
          </Press>
        )}
        <Typography size={18} type="semiBold">
          {title}
        </Typography>
      </View>
      <View style={{ flexDirection: 'row' }}>
        {filterIcon && (
          <Press
            style={{
              padding: 10,
              borderRadius: 10,
              borderWidth: 1,
              borderColor: colors.white,
              marginRight: 10,
            }} onPress={()=>{navigation.navigate('filterButton')}}>
            <Icon
              source={require('../../../Assets/images/filterA.png') }
              size={14}
              tintColor='white'
            />

          </Press>

        )}
        {shopIcon && (
          <Press 
            style={{
              padding: 10,
              borderRadius: 10,
              borderWidth: 1,
              borderColor: colors.white,
              marginRight:1,
            }} onPress={()=>{navigation.navigate('Soon')}}>
            <Icon
              source={require('../../../Assets/images/cart1.png')}
              size={14}
            />
          </Press>
        )}

        {close && (
          <Press
            style={{
              marginRight: 15,
              padding: 10,
              borderColor: colors.white,
              borderWidth: 1,
              alignSelf: 'flex-start',
              borderRadius: 10,
            }}
            onPress={() =>navigation.goBack()}>
            <Icon
              source={require('../../../Assets/images/close.png')}
              size={13}
              tintColor='white'
              imgStyle={{ resizeMode: 'contain' }}
            />
          </Press>
        )}


      </View>



    </View>
  );
};

export default AuthHeader;
