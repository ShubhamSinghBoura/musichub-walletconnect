import React, {useEffect, useState} from 'react';
import {Modal, StyleSheet, TouchableOpacity, View} from 'react-native';
import colors from '../../Constants/colors';

const ModalContainer = ({
  show = false,
  onClose = () => {},
  style = {},
  children,
  ...props
}) => {
  const [ShowModal, setShowModal] = useState(show);
  useEffect(() => {
    setShowModal(show);
  }, [show]);

  const onCloseHandler = () => {
    setShowModal(false);
    onClose();
  };

  return (
    <Modal
      transparent
      animationType="fade"
      visible={ShowModal}
      onRequestClose={onCloseHandler}
      {...props}>
      <View style={[styles.view, style]}>
        <TouchableOpacity style={styles.dismiss} onPress={onCloseHandler} />
        {children}
      </View>
    </Modal>
  );
};

export default ModalContainer;

const styles = StyleSheet.create({
  view: {
    backgroundColor: colors.black + '40',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  dismiss: {
    ...StyleSheet.absoluteFillObject,
  },
});
