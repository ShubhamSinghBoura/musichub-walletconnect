import {Platform, KeyboardAvoidingView, StyleSheet} from 'react-native';
import React from 'react';
import ScrollContainer from './ScrollContainer';
import ViewContainer, {containerStyle} from './ViewContainer';
import colors from '../../Constants/colors';
const keyboardVerticalOffset = Platform.OS === 'ios' ? 0 : 0;
const behavior = Platform.OS === 'ios' ? 'padding' : undefined;
const FormContainer = ({
  children,
  backgroundColor = colors.white,
  style = {},
  bottomView,
  bottomText,
  onBottomPress = () => {},
}) => {
  return (
    <ViewContainer>
    <KeyboardAvoidingView
      style={[containerStyle.container(backgroundColor), styles.keyboard]}
      behavior={behavior}
      keyboardVerticalOffset={keyboardVerticalOffset}>
      <ScrollContainer
      withBG
        bottomView={bottomView}
        bottomText={bottomText}
        onBottomPress={() => {
          onBottomPress();
        }}
        backgroundColor={backgroundColor}
        style={style}>
        {children}
      </ScrollContainer>
    </KeyboardAvoidingView>
    </ViewContainer>
  );
};

export default FormContainer;
const styles = StyleSheet.create({
  keyboard: {},
});
