import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Animated, { EasingNode } from 'react-native-reanimated';
import { STANDARD_WIDTH } from '../../Constants/layout';
import Typography from '../UI/Typography';

const ProgressBar = (props) => {
    const { value, width = STANDARD_WIDTH, activeColor = '#cb393a', height, backgroundColor = '#222', shouldAnimated = true } = props
    const animatedWidth1 = React.useRef(new Animated.Value(0)).current;
    const progress = () => {
        Animated.timing(animatedWidth1, {
            toValue: value,
            useNativeDriver: false,
            duration: 2000,
            easing: EasingNode.ease
        }).start();
    }

    React.useEffect(() => {
        progress()
    }, [value])

    const width1 = animatedWidth1.interpolate({
        inputRange: [0, 3],
        outputRange: [0, STANDARD_WIDTH - 20],
    })

    return (
        <View style={{ marginTop: 20, }} >

            <View style={{ flexDirection: 'row', width: width, alignSelf: 'center', marginBottom: 0 }} >
                <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end' }} >
                    <Typography size={18} type='medium'>1</Typography>
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end' }} >
                    <Typography size={18} type='medium'>2</Typography>
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end' }} >
                    <Typography size={18} type='medium'>3</Typography>
                </View>
            </View>
            <View style={{
                ...styles.progressView, overflow: "hidden", width: width, backgroundColor: backgroundColor, height: 4, alignSelf: 'center', elevation: 1, zIndex: 1
            }} >
                <Animated.View style={{ ...styles.progressView, backgroundColor: activeColor, height: 3, width: width1 }} />
            </View>
        </View>



    )
}

export default ProgressBar

const styles = StyleSheet.create({
    progressView: {
        borderRadius: 10
    }
})
