import { StyleSheet, Animated,ScrollView, View, TouchableOpacity, Alert,Image, Platform } from 'react-native';
import React, { useRef, useState } from 'react';
import ViewContainer, { containerStyle } from './ViewContainer';
import colors from '../../Constants/colors';
import { FULL_HEIGHT, FULL_WIDTH, STANDARD_WIDTH } from '../../Constants/layout';
import CommonButton from '../UI/CommonButton';
import Press from './Press';
import Icon from '../UI/Icon';
import { useNavigation } from '@react-navigation/core';
import  { Easing } from 'react-native-reanimated';
import images from '../../Constants/images';
import Typography from '../UI/Typography';
import { useAuth, useDrawer } from '../../Context/UserContext';

const ScrollContainer = ({
  children,
  backgroundColor = colors.black,
  style = {},
  contentStyle = {},
  withBG,
  showsVerticalScrollIndicator = false,
  bottomText,
  onBottomPress = () => { },
  bottomView,
  bottomButton=true,
  stickyHeaderIndices,
  contentContainerStyle
}) => {
  const [drawer,setDrawer]=useState(false)
const {   userDrawer } = useDrawer()
const {  updateIsAuth } = useAuth()
  // const [userData, setuserData] = useState(useDrawer);
  const [isOpen, setisOpen] = useState(false);
  const navigation = useNavigation();
  const animation = useRef(new Animated.Value(0)).current;
  
  const widthanimation = animation.interpolate({
    inputRange: [0, 1],
    outputRange: [
    FULL_HEIGHT,
      0,
    ],
    extrapolate: 'clamp',
  })

  const animatedStyles = {
    transform:[{translateY:widthanimation}]
  };
const [isSubRoute,setisSubRoute] = useState(false)
  const onLogout = () => {
    Alert.alert('Logout', 'Are you sure you want to logout ?', [
      {
        text: 'Cancel',
        onPress: () => console.log(''),
        style: 'cancel',
      },
      {
        text: 'OK',
        onPress: async () => {
          
          updateIsAuth(false);
        },
      },
    ]);
  };

  const toDoAnimation = () => {
   
    Animated.timing(animation, {
      toValue: isOpen ? 0 : 1,
      duration:  500,
      easing: Easing.circle,
      useNativeDriver: true
    }).start();
    setisOpen(!isOpen);
    setisSubRoute(false)
  };

  const onNavigation = route => {
    Animated.timing(animation, {
      toValue: 0,
      duration: 500,
      easing: Easing.circle,
      useNativeDriver: true,
    }).start();
    setisOpen(!isOpen);
    setisSubRoute(false)
    navigation.navigate(route);
  };

  return (
    <ViewContainer backgroundColor={backgroundColor} style={style}>
{/* {
  isOpen && */}

        <Animated.View
          style={[
            styles.drawer,
            animatedStyles,
            { backgroundColor: colors.white },
          ]}>  
          <View style={[styles.drawer,{ right: 3,bottom: 5,borderTopColor: colors.redBorderColor,borderLeftColor : colors.redBorderColor,borderTopWidth: 2,borderLeftWidth: 2}]} >
            <ScrollView
              contentContainerStyle={{ paddingBottom: 80 }}
              style={styles.card}
              
              showsVerticalScrollIndicator={false}>
              
                <View style={{ flex: 1, alignItems: 'center',}}>
                  <View style={{ flexDirection: 'row',alignSelf: 'center',alignItems: 'flex-start', marginVertical: 30}} >
                  <Icon round size={50} source={images.user} />
                  <TouchableOpacity
                    disabled
                    style={[styles.tab, { justifyContent: 'center' }]}
                    activeOpacity={0.99}>
                      <View>
                    <Typography size={18}  type={'medium'}>
                      Shaurya
                    </Typography>
                    <Typography textAlign={'center'} size={12}  type={'medium'}>
                      dummy1234@gmail.comm
                    </Typography>
                    </View>
                  </TouchableOpacity>
                  </View>
                   {userDrawer?.map((item, i) => {
                    return (
                      <View key={i} style={{ width: '100%',}}>
                        <View
                          key={i}
                            style={{ width: '100%', alignItems: 'center' }}>
                          <TouchableOpacity
                            key={i}
                            style={styles.tab}
                            activeOpacity={0.99}
                            onPress={() => onNavigation(item.routeName)}>
                            <Typography
                              type="medium"
                              size={16}
                              color={colors.white}>
                              {item.name}
                            </Typography>
                          </TouchableOpacity>
                        </View>
                       </View>
                    );
                  })} 
                  <TouchableOpacity
                    style={{...styles.tab,alignSelf: 'center'}}
                    activeOpacity={0.99}
                    onPress={() => onLogout()}>
                    <Typography type="medium" color={colors.redBorderColor} size={16} >
                      Logout
                    </Typography>
                  </TouchableOpacity>
                 </View>
                   </ScrollView>
                   </View>
        </Animated.View>
{/* } */}
      {
        withBG &&
        <Image source={require('../../../Assets/images/dotsbg.png')} style={{ zIndex: 0, height: FULL_HEIGHT * 0.45, width: FULL_WIDTH * 0.6, alignSelf: 'flex-end', position: 'absolute', resizeMode: 'cover', top: 0, right: 10 }} />
      }
      <ScrollView
        showsVerticalScrollIndicator={showsVerticalScrollIndicator}
        stickyHeaderIndices={stickyHeaderIndices}

        contentContainerStyle={contentContainerStyle}
        contentInsetAdjustmentBehavior={{
          zIndex: 9999,
          ...contentStyle
        }}
        style={[containerStyle.container(backgroundColor), styles.scroll]}>


        {children}
        {
          bottomView &&
          <View style={{ height: 150 }} />

        }
      </ScrollView>
      {
        (bottomView)&&
        <View style={{ height: 80, position: 'absolute', bottom: 0, zIndex: 99999, width: STANDARD_WIDTH, flexDirection: 'row', justifyContent: 'space-between', }} >
         {
              (bottomButton&& !isOpen) ? 
            
          <View style={{
            width: STANDARD_WIDTH - 80,
            alignSelf: 'flex-start',
            marginLeft: 20,
            backgroundColor: 'white',
            borderRadius: 10,
            height: 53,
            top: 5
          }}>
            
            <CommonButton
              text={bottomText}
              style={{
                padding: 0,
                width: STANDARD_WIDTH - 80,
                alignSelf: 'flex-start',
                borderTopWidth: 1.5,
                borderRightWidth: 1.5,
                borderColor: colors.redBorderColor,
                bottom: 8,
                left: 5
              }}
              onPress={() => {
                onBottomPress()
              }}
            />
            
          </View>
          : 
          <View style={{ width: STANDARD_WIDTH-80,backgroundColor: 'transparent' }} />
}

          <Icon
            source={require('../../../Assets/images/button.png')}
            size={60}
            touchable
              onPress={() => toDoAnimation()}

            imgStyle={{ marginLeft: 25,zIndex: 999999 }}
          />
        </View>
      }

    </ViewContainer>
  );
};

export default ScrollContainer;

const styles = StyleSheet.create({
  scroll: {
    width: STANDARD_WIDTH,
    alignSelf: 'center',
    paddingTop: Platform.OS == 'android'? 40 : 10,
    backgroundColor: 'transparent'
  },
  buttonContainer: {
    justifyContent: 'center',
    borderRadius: 5,
    flex: 1,
  },
  backgroundImageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  buttonIcon: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  mainContainer: (r, l, t, b) => {
    return {
      position: 'absolute',
      right: 0,
      bottom: 0,
      borderRadius: 5,
      top: t,
      left: 0,
      height: 80,
    };
  },
  card: {
    width: '90%',
    alignSelf: 'center',
    marginVertical: 10,
  },
  tab: {
    // width: '100%',
    marginVertical: 5,
    height: 40,
    justifyContent: 'space-between',
    paddingHorizontal: 13,
    flexDirection: 'row',
    alignItems: 'center'
  },
  subtab: {
    width: '100%',
    marginBottom: 15,
    justifyContent: 'center',
    paddingLeft: 25,
    paddingVertical: 10
  },
  drawer: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    zIndex: 9999,
    backgroundColor: colors.inputBox,
    borderTopLeftRadius: 8,
    elevation: 20,
    width:FULL_WIDTH*0.8,
    height:FULL_HEIGHT*0.65,
    borderRadius: 10,
    marginBottom: 5,
    marginRight: 5
  },
  linearGradient: {
    width: '100%',
    height: 1,
  },
  imageStyle: {
    width: '100%',
  },
});
