import React, {useContext, useState} from 'react';
import { USER_PRODUCER } from '../Constants/userDrawerType';

export const UserContext = React.createContext({
  isAuth: false,
  updateIsAuth: () => {},
  userData: {},
  updateUserData: () => {},
  initialDashboard:'',
  updateinitialDashboard:()=>{},
  userDrawer: [],
  updateUserDrawer: ()=>{}

});
export const UserProvider = ({children}) => {
  const [isAuth, setIsAuth] = useState(false);
  const [userData, setUserData] = useState({});
  const [initialDashboard, setinitialDashboard] = useState('')
  const [userDrawer,setUserDrawer] = useState(USER_PRODUCER)
  return (
    <UserContext.Provider
      value={{
        isAuth,
        updateIsAuth: setIsAuth,
        userData,
        updateUserData: setUserData,
        initialDashboard,
        updateinitialDashboard:setinitialDashboard,
        userDrawer,
        updateUserDrawer : setUserDrawer
      }}>
      {children}
    </UserContext.Provider>
  );
};

export const useAuth = () => {
  const {isAuth, updateIsAuth} = useContext(UserContext);
  return {isAuth, updateIsAuth};
};
export const useUser = () => {
  const {userData, updateUserData} = useContext(UserContext);
  return [userData, updateUserData];
};
export const useDashboard = () => {
  const {initialDashboard, updateinitialDashboard} = useContext(UserContext);
  return {initialDashboard, updateinitialDashboard};
};
export const useDrawer = () => {
  const {userDrawer, updateUserDrawer} = useContext(UserContext);
  return {userDrawer, updateUserDrawer};
};
