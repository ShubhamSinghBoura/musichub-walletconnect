import SimpleToast from 'react-native-simple-toast';
const VALIDATE = {
    EMAIL: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    ALPHABET_ONLY: /^[a-zA-Z \s]*$/,
    NUMBER: /[0-9]$/,
    MOBILE: /^[0-9]{1,20}$/,
    STREET: /^[a-zA-Z0-9 '-.~!@#$%^&*()_+={}[];':"<>,.\s]*$/,
    PASSWORD: /[d\-_\s]+$/,
};

export const validators = {
    checkAlphabet: (name, min, max, value) => {
        var min = min || 2;
        var max = max || 30;
        if (value) {
            if (!VALIDATE.ALPHABET_ONLY.test(value)) {
                // SimpleToast();
                return `${name} is Invalid.`;
            } else if (value.length < min || value.length > max) {
                // SimpleToast();
                return `${name} must be between ${min} to ${max} Characters.`;
            }
            return null;
        } else {
            // SimpleToast();
            return `${name} is Required.`;
        }
    },

    checkEmail: (name, value) => {
        if (value) {
            if (!VALIDATE.EMAIL.test(value)) {
                return `${name} is Invalid.`;
            } else {
                return null
            }
        } else {
            return `${name} is Required.`;
        }
        return null;
    },

    checkNumber: (name, value) => {

        if (value) {
            if (!VALIDATE.NUMBER.test(value)) {

                return `${name} is Invalid.`;
            }
            return null;
        } else {
            // SimpleToast();
            return `${name} is Required.`;
        }
    },

    checkPhoneNumberWithFixLength: (name, max, value) => {
        var max = max || 10;
        if (value) {
            if (!VALIDATE.MOBILE.test(value)) {
                SimpleToast(`${name} is Invalid.`);
                return false;
            } else if (value.length != max) {
                SimpleToast(`${name} should be ${max} digits.`);
                return false;
            }
            return true;
        } else {
            SimpleToast(`${name} is Required.`);
            return false;
        }
    },

    checkOptionalPhoneNumberWithFixLength: (name, max, value) => {
        var max = max || 10;
        if (value) {
            if (!VALIDATE.MOBILE.test(value)) {
                SimpleToast(`${name} is Invalid.`);
                return false;
            } else if (value.length != max) {
                SimpleToast(`${name} should be ${max} digits.`);
                return false;
            }
            return true;
        } else {
            return true;
        }
    },

    checkPhoneNumber: (name, min, max, value) => {
        var min = min || 7;
        var max = max || 15;
        if (value) {
            if (!VALIDATE.MOBILE.test(value)) {
                // SimpleToast();
                return `${name} is Invalid.`;
            }
            // else if (value.length < min || value.length > max) {
            //   // SimpleToast(`${name} should be greater than ${min - 1} digits.`);
            //   return `${name} should be greater than ${min - 1} digits.`;
            // }
            return null;
        } else {
            // SimpleToast(`${name} is Required.`);
            return `${name} is Required.`;
        }
    },

    checkNotNull: (name, min, max, value) => {
        var min = min || 5;
        var max = max || 40;
        if (value) {
            if (value.length < min || value.length > max) {
                SimpleToast(`${name} must be between ${min} to ${max} Characters.`);
                return false;
            }
            return true;
        } else {
            SimpleToast(`${name} is Required.`);
            return false;
        }
    },

    checkRequire: (name, value) => {
        if (value) {
            return null;
        } else {
            return ` ${name} is Required.  `;
        }
    },
    checkMultiple: (name, value) => {

        if (value?.length > 0) {
            return null
        } else {
            return `${name} is required.`
        }
        // if (value) {
        //     return null;
        // } else {
        //     return `Please enter ${name}`;
        // }
    },

    checkPassword: (name, value) => {

        if (value) {
            if (VALIDATE.PASSWORD.test(value)) {
                return `${name} is Invalid.`;
            }
            //  else if (value.length < min || value.length > max) {
            //   return `${name} entered must be between ${min} to ${max} Characters.`;
            // }
            return '';
        } else {
            return `${name} is Required.`;
        }
    },

    checkMatch: (name, value, name2, value2) => {
        var min = min || 5;
        var max = max || 40;
        if (value == value2) {
            return true;
        } else {
            SimpleToast(`${name} and ${name2} should be same.`);
            return false;
        }
    },

    checkStreet: (name, min, max, value) => {
        var min = min || 7;
        var max = max || 15;
        if (value) {
            if (VALIDATE.STREET.test(value)) {
                SimpleToast(`${name} is Invalid.`);
                return false;
            } else if (value.length < min || value.length > max) {
                SimpleToast(
                    `${name} entered must be between ${min} to ${max} Characters.`,
                );
                return false;
            }
            return true;
        } else {
            SimpleToast(`${name} is Required.`);
            return false;
        }
    },
};
