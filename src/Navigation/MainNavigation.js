/*
    READ howto.md FOR MORE INFO
*/
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';

import { DrawerNavigation } from './DrawerNavigation';
import { AuthStack, HomeStack } from './StackNavigation';
import { useAuth } from '../Context/UserContext';
import { Platform, StatusBar, View } from 'react-native';
import colors from '../Constants/colors';
import { FULL_WIDTH } from '../Constants/layout';

const MainNavigation = () => {
  //State
  const { isAuth } = useAuth();
  //Effect

  //Function

  return (
    <View style={{ flex: 1, }} >
    {
      Platform.OS == 'android'?
      <StatusBar translucent={true} backgroundColor={'transparent'} barStyle='light-content' />
    :

    
    <View style={{ width: FULL_WIDTH, height: 30, backgroundColor: colors.black }} >
      <StatusBar translucent={true} backgroundColor={'transparent'} barStyle='light-content' />
      </View>
}
    <NavigationContainer theme={{colors:{background:colors.bgColor}}}>
      {/*use any state management library for authenticating user
            Context ,REDUX etc
            in this version we are using basic useState
            soon we will have more versions with REDUX and Context*/}
      {isAuth ? <HomeStack /> : <AuthStack />}
    </NavigationContainer>
 </View>
  );
};
export default MainNavigation;
