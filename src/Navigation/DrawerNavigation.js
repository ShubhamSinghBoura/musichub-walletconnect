/*
    READ howto.md FOR MORE INFO
*/

import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { View, ImageBackground } from 'react-native';
import { FULL_WIDTH, FULL_HEIGHT } from '../Constants/layout';
const Drawer = createDrawerNavigator();

const commonOptions = {};

export const DrawerNavigation = () => {
  return (

    <Drawer.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Drawer.Screen
        name="HomeDrawer"
        getComponent={() => require('./TabNavigation').TabNavigation}
        options={{
          ...commonOptions,
          title: 'Home',
        }}
      />
      <Drawer.Screen
        name="ProfileDrawer"
        getComponent={() => require('./StackNavigation').ProfileStack}
        options={{
          ...commonOptions,
          title: 'Profile',
        }}
      />
    </Drawer.Navigator>
  );
};
