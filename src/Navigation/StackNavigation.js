/*Pp
    READ howto.md FOR MORE INFO
*/

import React from 'react';
import { CardStyleInterpolators, createStackNavigator } from '@react-navigation/stack';
import Explore from '../Screen/Private/Explore';
import { useDashboard } from '../Context/UserContext';
const Stack = createStackNavigator();

const commonOptions = { headerShown: false , cardStyleInterpolator:CardStyleInterpolators.forHorizontalIOS};

//NOTE Always use name with respected navigation type like AuthStack, AuthTab , AuthDrawer
export const AuthStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        ...commonOptions,
      }}>
      <Stack.Screen
        name="Login"
        //NOTE never use  component={} for injecting file use getComponent for better speed and performance
        getComponent={() => require('../Screen/Public/Login').default}
        options={{
          ...commonOptions,
          
        }}
      />

      <Stack.Screen
        name="SelectUsers"
        getComponent={() => require('../Screen/Public/SelectUsers').default}
        options={{
          ...commonOptions,
        }}
      />
      <Stack.Screen
        name="Walkthrough"
        getComponent={() => require('../Screen/Public/Walkthrough').default}
        options={{
          ...commonOptions,
        }}
      />



    </Stack.Navigator>
  );
};

export const HomeStack = () => {
  const { initialDashboard } = useDashboard()
  console.log(initialDashboard);
  return (
    <Stack.Navigator
      initialRouteName={initialDashboard}
      screenOptions={{
        ...commonOptions,
      }}>

      <Stack.Screen
        name="HomeNFT"
        getComponent={() =>
          require('../Screen/Private/HomeNFT').default
        }
        options={{
          ...commonOptions,
        }}
      />
      <Stack.Screen
        name="HomeMusicPR"
        getComponent={() =>
          require('../Screen/Private/HomeMusicPR').default
        }
        options={{
          ...commonOptions,
        }}
      />
      <Stack.Screen
        name="HomeMusicRaver"
        getComponent={() =>
          require('../Screen/Private/Raver/HomeMusicRaver').default
        }
        options={{
          ...commonOptions,
        }}
      />
      <Stack.Screen
        name="CreateTrack"
        getComponent={() => require('../Screen/Private/CreateTrack').default}

        options={{
          ...commonOptions,
        }}
      />
       <Stack.Screen
        name="Soon"
        getComponent={() => require('../Screen/Public/Soon').default}

        options={{
          ...commonOptions,
        }}
      />
      <Stack.Screen
        name="Producer"
        getComponent={() => require('../Screen/Private/Producer/Producer').default}

        options={{
          ...commonOptions,
        }}
      />
      <Stack.Screen
        name="CreateNft1"
        getComponent={() =>
          require('../Screen/Private/CreateNFt/CreateNft1').default
        }
        options={{
          ...commonOptions,
        }}
      />
      <Stack.Screen
        name="CreateNft2"
        getComponent={() =>
          require('../Screen/Private/CreateNFt/CreateNft2').default
        }
        options={{
          ...commonOptions,
        }}
      />
      <Stack.Screen
        name="CreateNft3"
        getComponent={() =>
          require('../Screen/Private/CreateNFt/CreateNft3').default
        }
        options={{
          ...commonOptions,
        }}
      />
      <Stack.Screen
        name="NFTS"
        getComponent={() =>
          require('../Screen/Private/CreateNFt/NFTS').default
        }
        options={{
          ...commonOptions,
        }}
      />
      <Stack.Screen
        name="MyPlayList"
        getComponent={() =>
          require('../Screen/Private/Playlist/MyPlayList').default
        }
        options={{
          ...commonOptions,
        }}
      />
      <Stack.Screen
        name="ViewMusic"
        getComponent={() =>
          require('../Screen/Private/Playlist/ViewMusic').default
        }
        options={{
          ...commonOptions,
        }}
      />
      <Stack.Screen
        name="Fevorites"
        getComponent={() =>
          require('../Screen/Private/Favorites').default
        }
        options={{
          ...commonOptions,
        }}
      />
      <Stack.Screen
        name="Explore"
        getComponent={() =>
          require('../Screen/Private/Explore').default
        }
        options={{
          ...commonOptions,
        }}

      />

      <Stack.Screen
        name="Notification"
        getComponent={() =>
          require('../Screen/Private/Notification').default
        }
        options={{
          ...commonOptions,
        }}
      />
      <Stack.Screen
        name="Transaction"
        getComponent={() =>
          require('../Screen/Private/Transaction').default
        }
        options={{
          ...commonOptions,
        }}
      />
      <Stack.Screen
        name="Artist"
        getComponent={() =>
          require('../Screen/Private/Artist').default
        }
        options={{
          ...commonOptions,
        }}
      />

      <Stack.Screen
        name="myAccount"
        getComponent={() =>
          require('../Screen/Private/myAccount').default
        }
        options={{
          ...commonOptions,
        }}
      />

      <Stack.Screen
        name="Artistdetails"
        getComponent={() =>
          require('../Screen/Private/Artistdetails').default
        }
        options={{
          ...commonOptions,
        }}
      />



      <Stack.Screen
        name="ChooseMusic"
        getComponent={() =>
          require('../Screen/Private/ChooseMusic').default
        }
        options={{
          ...commonOptions,
        }}
      />


      <Stack.Screen
        name="filterButton"
        getComponent={() =>
          require('../Screen/Private/filterButton').default
        }
        options={{
          ...commonOptions,
          cardStyleInterpolator:CardStyleInterpolators.forVerticalIOS
        }}
      />

      <Stack.Screen
        name="MusicPlayer"
        getComponent={() =>
          require('../Screen/Private/Raver/MusicPlayer').default
        }
        options={{
          ...commonOptions,
        }}
      />

    </Stack.Navigator>




  );
};

export const ProfileStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        ...commonOptions,
      }}>
      <Stack.Screen
        name="Profile"
        getComponent={() =>
          require('../Screen/Private/Profile/Profile').default
        }
        options={{
          ...commonOptions,
        }}
      />
    </Stack.Navigator>
  );
};
