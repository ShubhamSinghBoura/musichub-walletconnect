/*
    READ howto.md FOR MORE INFO
*/

import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
const Tab = createBottomTabNavigator();

const commonOptions = {};

export const TabNavigation = ({initialRouteName = 'HomeTab'}) => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName={initialRouteName}>
      <Tab.Screen
        name="HomeTab"
        getComponent={() => require('./StackNavigation').HomeStack}
        options={{
          ...commonOptions,
        }}
      />
      <Tab.Screen
        name="ProfileTab"
        getComponent={() => require('./StackNavigation').ProfileStack}
        options={{
          ...commonOptions,
        }}
      />
    </Tab.Navigator>
  );
};
